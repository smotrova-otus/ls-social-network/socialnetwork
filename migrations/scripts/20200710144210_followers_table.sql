-- +migrate Up

create table followers
(
    profile_id  varchar(36) not null,
    follower_id varchar(36) not null,

    constraint followers_pk
        primary key (profile_id, follower_id)
) ENGINE=InnoDB;

alter table followers
    add constraint followers_profiles__fk_profile_id
        foreign key (profile_id) references profiles (`id`)
            on update cascade on delete cascade;

alter table followers
    add constraint followers_profiles__fk_follower_id
        foreign key (follower_id) references profiles (`id`)
            on update cascade on delete cascade;

-- +migrate Down

DROP TABLE IF EXISTS followers;
