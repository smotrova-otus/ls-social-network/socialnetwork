-- +migrate Up

create index profiles_first_name_last_name_index
    on profiles (first_name, last_name);

-- +migrate Down

drop index profiles_first_name_last_name_index on profiles;