-- +migrate Up

create table profiles
(
    id         varchar(36)   not null,
    first_name varchar(255)  not null,
    last_name  varchar(255)  not null,
    city       varchar(255)  not null,
    interests  varchar(1024) not null,
    gender     varchar(10)   not null,
    birth_date DATE,
    constraint profiles_pk
        primary key (id)
) ENGINE=InnoDB;

-- +migrate Down

DROP TABLE IF EXISTS profiles;
