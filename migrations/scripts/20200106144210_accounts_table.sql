-- +migrate Up

create table accounts
(
    id varchar(36) not null,
    login varchar(255) not null,
    password_hash BLOB not null,
    password_salt BLOB not null,
    constraint accounts_pk
        primary key (id)
) ENGINE=InnoDB;

create unique index accounts_login_uindex
    on accounts (login);

-- +migrate Down

DROP TABLE IF EXISTS accounts;

