package config

import "time"

type DBConfig struct {
	Dialect  string `yaml:"dialect" validate:"required"`
	Host     string `yaml:"host" validate:"required"`
	Port     int    `yaml:"port" validate:"required"`
	User     string `yaml:"user" validate:"required"`
	Password string `yaml:"password" validate:"required"`
	Name     string `yaml:"name" validate:"required"`
}

type Config struct {
	Transport struct {
		Http struct {
			Address string `yaml:"address" validate:"required"`
		} `yaml:"http" validate:"required"`
	} `yaml:"transport" validate:"required"`
	Database struct {
		Master *DBConfig `yaml:"master"`
		Slave  *DBConfig `yaml:"slave"`
	} `yaml:"database" validate:"required"`
	Auth struct {
		Key      string        `yaml:"key" validate:"required"`
		Ttl      time.Duration `yaml:"ttl" validate:"required"`
		Audience string        `yaml:"audience" validate:"required"`
		Issuer   string        `yaml:"issuer" validate:"required"`
	} `yaml:"auth" validate:"required"`
	Logger struct {
		Level  string `yaml:"level"`
		Caller bool   `yaml:"caller"`
	} `yaml:"logger"`
}
