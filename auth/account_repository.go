package auth

import (
	"database/sql"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/database"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type AccountRepository interface {
	Exists(login string) (bool, error)
	FindByLogin(login string) (*domain.Account, error)
	FindByID(id uuid.UUID) (*domain.Account, error)
	Insert(account *domain.Account) error
	Update(account *domain.Account) error
	FindAllWithFilter(offset int64, limit int, f filter) ([]domain.Account, error)
}

// repository represent account db account Repository.
type AccountRepositoryImpl struct {
	db database.QueryerExecer
}

// NewAccountRepository is constructor.
func NewAccountRepository(db database.QueryerExecer) *AccountRepositoryImpl {
	return &AccountRepositoryImpl{db: db}
}

// Exists checks existing account.
func (r AccountRepositoryImpl) Exists(login string) (bool, error) {
	var finded string
	err := sqlx.Get(r.db, &finded, "select 1 from accounts where login=? limit 1", login)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// FindByLogin finds account by login.
func (r AccountRepositoryImpl) FindByLogin(login string) (account *domain.Account, err error) {
	account = &domain.Account{}
	err = sqlx.Get(r.db, account, "select * from accounts where login=? limit 1", login)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return account, nil
}

// FindAllWithFilter finds accounts by filter.
func (r AccountRepositoryImpl) FindAllWithFilter(offset int64, limit int, f filter) ([]domain.Account, error) {
	accounts := make([]domain.Account, 0)
	var err error
	var args []interface{}
	query := "select * from accounts"
	if f.Login != "" {
		query = query + " where login=?"
		args = append(args, f.Login)
	}
	if limit != -1 {
		query = query + " limit ?, ?"
		args = append(args, limit, offset)
	}
	err = sqlx.Select(r.db, &accounts, query, args...)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return accounts, nil
}

// FindByID finds account by id.
func (r AccountRepositoryImpl) FindByID(id uuid.UUID) (account *domain.Account, err error) {
	account = &domain.Account{}
	err = sqlx.Get(r.db, account, "select * from accounts where id=? limit 1", id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return account, nil
}

func (r AccountRepositoryImpl) Insert(account *domain.Account) error {
	_, err := r.db.Exec(
		"insert into accounts (id, login, password_hash, password_salt) values(?,?,?,?)",
		account.ID,
		account.Login,
		account.PasswordHash,
		account.PasswordSalt,
	)
	if err != nil {
		return err
	}
	return nil
}

func (r AccountRepositoryImpl) Update(account *domain.Account) error {
	res, err := r.db.Exec(
		"update accounts set login=?, password_hash=?, password_salt=? where id=?",
		account.Login,
		account.PasswordHash,
		account.PasswordSalt,
		account.ID,
	)
	if err != nil {
		return err
	}
	c, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if c < 1 {
		return database.ErrNoFound
	}
	return nil
}
