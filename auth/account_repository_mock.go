package auth

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type accountRepositoryMock struct {
	mock.Mock
}

// newAccountRepositoryMock is a constructor.
func newAccountRepositoryMock() *accountRepositoryMock {
	return &accountRepositoryMock{}
}

// Exists implementation.
func (m *accountRepositoryMock) Exists(login string) (bool, error) {
	results := m.Called(login)
	return results.Get(0).(bool), results.Error(1)
}

// FindByLogin implementation.
func (m *accountRepositoryMock) FindByLogin(login string) (*domain.Account, error) {
	results := m.Called(login)
	return results.Get(0).(*domain.Account), results.Error(1)
}

// FindByID implementation.
func (m *accountRepositoryMock) FindByID(id uuid.UUID) (*domain.Account, error) {
	results := m.Called(id)
	return results.Get(0).(*domain.Account), results.Error(1)
}

// Insert implementation.
func (m *accountRepositoryMock) Insert(account *domain.Account) error {
	results := m.Called(account)
	return results.Error(0)
}

// Update implementation.
func (m *accountRepositoryMock) Update(account *domain.Account) error {
	results := m.Called(account)
	return results.Error(0)
}

// FindAllWithFilter implementation.
func (m *accountRepositoryMock) FindAllWithFilter(offset int64, limit int, f filter) ([]domain.Account, error) {
	results := m.Called(offset, limit, f)
	return results.Get(0).([]domain.Account), results.Error(1)
}

// setExists set mock for method Exists.
func (m *accountRepositoryMock) setExists(login string, exists bool, err error) {
	m.On("Exists", login).
		Return(exists, err)
}

// setFindByLogin set mock for method FindByLogin.
func (m *accountRepositoryMock) setFindByLogin(login string, acc *domain.Account, err error) {
	m.On("FindByLogin", login).
		Return(acc, err)
}

// setFindByID set mock for method FindByID.
func (m *accountRepositoryMock) setFindByID(id uuid.UUID, acc *domain.Account, err error) {
	m.On("FindByID", id).
		Return(acc, err)
}

// setInsert set mock for method Insert.
func (m *accountRepositoryMock) setInsert(acc *domain.Account, err error) {
	m.On("Insert", acc).
		Return(err)
}

// setUpdate set mock for method Update.
func (m *accountRepositoryMock) setUpdate(acc *domain.Account, err error) {
	m.On("Update", acc).
		Return(err)
}

// setFindAllWithFilter set mock for method FindAllWithFilter.
func (m *accountRepositoryMock) setFindAllWithFilter(
	offset int64,
	limit int,
	f filter,
	accounts []domain.Account,
	err error) {
	m.On("FindAllWithFilter", offset, limit, f).
		Return(accounts, err)
}
