package auth

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"strings"
)

type Endpoints struct {
	CreateAccountEndpoint endpoint.Endpoint
	AuthenticateEndpoint  endpoint.Endpoint
	ReadEndpoint          endpoint.Endpoint
	ListEndpoint          endpoint.Endpoint
}

// MakeServerEndpoints returns an Endpoints struct where each endpoint invokes
// the corresponding method on the provided service. Useful in a profilesvc
// server.
func MakeServerEndpoints(s Service, params auth.EndpointParams) Endpoints {
	authMiddleware := auth.AuthenticationMiddleware(params)

	return Endpoints{
		CreateAccountEndpoint: MakeCreateAccountEndpoint(s),
		AuthenticateEndpoint:  MakeAuthenticateEndpoint(s),
		ReadEndpoint:          authMiddleware(MakeReadEndpoint(s)),
		ListEndpoint:          authMiddleware(MakeListEndpoint(s)),
	}
}

type createAccountRequest struct {
	CreateDTO
}
type createAccountResponse struct {
	AccountDTO
	Err error `json:"err,omitempty"`
}

type AccountDTO struct {
	ID    uuid.UUID `json:"id"`
	Login string    `json:"login"`
}

// Failed implements Failer.
func (r createAccountResponse) Failed() error {
	return r.Err
}

// MakeCreateAccountEndpoint returns an endpoint via the passed service.
func MakeCreateAccountEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(createAccountRequest)
		acc, err := s.Create(ctx, req.CreateDTO)
		if err != nil {
			return nil, err
		}
		return createAccountResponse{
			AccountDTO: AccountDTO{
				ID:    acc.ID,
				Login: acc.Login,
			},
		}, nil
	}
}

type authenticateRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type authenticateResponse struct {
	Token string `json:"token"`
	Err   error  `json:"err,omitempty"`
}

// Failed implements Failer.
func (r authenticateResponse) Failed() error {
	return r.Err
}

// MakeAuthenticateEndpoint returns an endpoint via the passed service.
func MakeAuthenticateEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(authenticateRequest)
		token, err := s.Authenticate(ctx, LoginDTO{
			Login:    req.Login,
			Password: req.Password,
		})
		if err != nil {
			return nil, err
		}
		return authenticateResponse{
			Token: token,
		}, nil
	}
}

// readRequest collects the request parameters for the Read method.
type readRequest struct {
	ID uuid.UUID
}

// readResponse  collects the response parameters for the Read method.
type readResponse struct {
	*domain.Account
	Err error `json:"err,omitempty"`
}

// Failed implements Failer.
//kitgen:autogenerate
func (r readResponse) Failed() error {
	return r.Err
}

// MakeReadEndpoint returns an endpoint that invokes Read on the service.
//kitgen:autogenerate
func MakeReadEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(readRequest)
		account, err := s.Read(ctx, req.ID)
		return readResponse{
			Account: account,
			Err:     err,
		}, nil
	}
}

// listRequest collects the request parameters for the List method.
type listRequest struct {
	Login  string `json:"-"`
	Limit  int    `json:"-"`
	Offset int64  `json:"-"`
}

// listResponse collects the response parameters for the List method.
type listResponse struct {
	Accounts []domain.Account `json:"data"`
	Err      error            `json:"err,omitempty"`
}

// Failed implements Failer.
func (r listResponse) Failed() error {
	return r.Err
}

// MakeListEndpoint returns an endpoint that invokes List on the service.
func MakeListEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listRequest)
		req.Login = strings.ToLower(req.Login)
		accounts, err := s.List(ctx, req.Limit, req.Offset, filter{
			req.Login,
		})
		return listResponse{
			Accounts: accounts,
			Err:      err,
		}, nil
	}
}
