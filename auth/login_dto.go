package auth

import (
	"strings"
)

type LoginDTO struct {
	Login    string
	Password string
}

func (dto *LoginDTO) modify() {
	dto.Password = strings.Trim(dto.Password, " ")
	dto.Login = strings.Trim(dto.Login, " ")
	dto.Login = strings.ToLower(dto.Login)
}
