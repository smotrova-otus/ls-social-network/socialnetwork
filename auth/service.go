package auth

import (
	"bytes"
	"context"
	"fmt"

	"net/http"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/passwordhash"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth/token"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type Service interface {
	List(ctx context.Context, limit int, offset int64, filter filter) ([]domain.Account, error)
	Create(ctx context.Context, dto CreateDTO) (result *domain.Account, err error)
	Read(ctx context.Context, id uuid.UUID) (result *domain.Account, err error)
	Authenticate(ctx context.Context, dto LoginDTO) (token string, err error)
}

type service struct {
	tokenCreator      token.Creator
	accountRepository AccountRepository
	passwordHasher    passwordhash.PasswordHasher
	uuidGenerator     uuid.Generator
}

var (
	errAccountAlreadyExists = &errors.Error{
		Err:    "account already exists",
		Code:   "ACCOUNT_ALREADY_EXISTS",
		Status: http.StatusBadRequest,
	}
	errBadCredentials = &errors.Error{
		Err:    "bad credentials",
		Code:   "BAD_CREDENTIALS",
		Status: http.StatusUnauthorized,
	}
)

func (s service) Create(ctx context.Context, dto CreateDTO) (result *domain.Account, err error) {
	logger := log.Must(ctx)
	logger.WithField("method", "Create")
	dto.modify()
	err = dto.validate()
	if err != nil {
		logger.
			WithField("err", err).
			Warnf("create account failed: validation error (login: %s)", dto.Login)
		return nil, err
	}
	ex, err := s.accountRepository.Exists(dto.Login)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("create account failed: check existing account error (login: %s)", dto.Login)
		return nil, fmt.Errorf("check existing account error: %w", err)
	}
	if ex {
		logger.
			WithField("err", err).
			Warnf("create account failed: account already exists (login: %s)", dto.Login)
		return nil, errAccountAlreadyExists
	}
	ac := convertFromCreateDto(dto)
	ac.ID = s.uuidGenerator.New()
	ac.PasswordSalt, err = s.passwordHasher.MakeSalt()
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("create account failed: generate password salt error (login: %s)", dto.Login)
		return nil, fmt.Errorf("generate password salt failed: %w", err)
	}
	ac.PasswordHash, err = s.passwordHasher.CreateHash([]byte(dto.Password), ac.PasswordSalt)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("create account failed: generate password hash error (login: %s)", dto.Login)
		return nil, fmt.Errorf("generate password hash failed: %w", err)
	}
	err = s.accountRepository.Insert(&ac)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("create account failed: save in storage error (login: %s)", dto.Login)
		return nil, fmt.Errorf("save account error: %w", err)
	}
	logger.
		Infof("create account succeeded (login: %s)", dto.Login)
	return &ac, nil
}

func (s service) Authenticate(ctx context.Context, dto LoginDTO) (token string, err error) {
	logger := log.Must(ctx)
	logger.WithField("method", "Authenticate")
	dto.modify()
	account, err := s.accountRepository.FindByLogin(dto.Login)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("authenticate account failed: find account in storage error (login: %s)", dto.Login)
		return "", fmt.Errorf("find account in storage error: %w", err)
	}
	if account == nil {
		logger.
			Warnf("authenticate account failed: account not found (login: %s)", dto.Login)
		return "", errBadCredentials
	}
	hash, err := s.passwordHasher.CreateHash([]byte(dto.Password), account.PasswordSalt)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("authenticate account failed: generate password hash error (login: %s)", dto.Login)
		return "", fmt.Errorf("generate password hash failed: %w", err)
	}
	if !bytes.Equal(hash, account.PasswordHash) {
		logger.
			Warnf("authenticate account failed: invalid password (login: %s)", dto.Login)
		return "", errBadCredentials
	}

	token, err = s.tokenCreator.MakeToken(*account)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("authenticate account failed: generate token error (login: %s)", dto.Login)
		return "", fmt.Errorf("generate token failed:%w", err)
	}

	logger.
		Infof("authenticate account succeeded (login: %s)", account.Login)
	return token, nil
}

func (s service) Read(ctx context.Context, id uuid.UUID) (result *domain.Account, err error) {
	logger := log.Must(ctx)
	logger.WithField("method", "Read")
	account, err := s.accountRepository.FindByID(id)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("read account failed: find account in storage error (id: %v)", id)
		return nil, fmt.Errorf("find account in storage error: %w", err)
	}
	if account == nil {
		logger.
			Warnf("read account failed: account not found (id: %v)", id)
		return nil, errors.ErrNotFound
	}

	logger.
		Infof("read account succeeded (id: %v, login: %s)", id, account.Login)
	return account, nil
}

// List returns list of user accounts.
func (s *service) List(
	ctx context.Context, limit int, offset int64, filter filter,
) ([]domain.Account, error) {
	logger := log.Must(ctx)
	logger.WithField("method", "List")
	if limit == 0 {
		limit = -1
	}
	accounts, err := s.accountRepository.FindAllWithFilter(
		offset,
		limit,
		filter,
	)
	if err != nil {
		logger.WithError(err).Error("get accounts list failed: get list from repository error")
		return nil, err
	}
	logger.Info("get accounts list succeeded")
	return accounts, nil
}

func NewService(
	tokenCreator token.Creator,
	repository AccountRepository,
	hasher passwordhash.PasswordHasher,
	uuidGenerator uuid.Generator,
) Service {
	return &service{
		tokenCreator:      tokenCreator,
		accountRepository: repository,
		passwordHasher:    hasher,
		uuidGenerator:     uuidGenerator,
	}
}
