package auth

import "github.com/google/wire"

var WireSet = wire.NewSet(
	NewService,
	NewHTTPHandlerSet,
	MakeServerEndpoints,
	wire.Bind(new(AccountRepository), new(*AccountRepositoryImpl)),
	NewAccountRepository,
	wire.Struct(new(HTTPHandlerParams), "*"),
)
