package auth

import (
	"net/http"
	"strings"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
)

type CreateDTO struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

func (dto *CreateDTO) modify() {
	dto.Password = strings.Trim(dto.Password, " ")

	dto.Login = strings.Trim(dto.Login, " ")
	dto.Login = strings.ToLower(dto.Login)
}

var (
	errEmptyLogin = &errors.Error{
		Err:    "empty id",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
	errInvalidLoginLength = &errors.Error{
		Err:    "invalid id length",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
	errEmptyPassword = &errors.Error{
		Err:    "empty password",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
	errInvalidPasswordLength = &errors.Error{
		Err:    "invalid password length",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
)

func (dto *CreateDTO) validate() error {
	if dto.Login == "" {
		return errEmptyLogin
	}
	if len(dto.Login) <= 3 {
		return errInvalidLoginLength
	}
	if dto.Password == "" {
		return errEmptyPassword
	}
	if len(dto.Password) <= 3 {
		return errInvalidPasswordLength
	}
	return nil
}
