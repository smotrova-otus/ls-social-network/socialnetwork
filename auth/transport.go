package auth

import (
	"context"
	"encoding/json"
	"github.com/go-kit/kit/auth/jwt"
	kitendpoint "github.com/go-kit/kit/endpoint"
	kitlogrus "github.com/go-kit/kit/log/logrus"
	"github.com/go-kit/kit/transport"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	pkgerrors "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
	pkghttp "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/http"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"net/http"
	"strconv"
	"strings"

	kithttp "github.com/go-kit/kit/transport/http"
)

// HTTPHandlerParams provides params for handlers service options.
type HTTPHandlerParams struct {
	Logger *logrus.Entry
}

func getHTTPHandlerOptions(name string, params HTTPHandlerParams) []kithttp.ServerOption {
	logger := kitlogrus.NewLogrusLogger(params.Logger)

	return []kithttp.ServerOption{
		kithttp.ServerBefore(log.NewLoggerRequestMiddleware(params.Logger)),
		kithttp.ServerBefore(jwt.HTTPToContext()),
		kithttp.ServerErrorEncoder(pkghttp.ErrorEncoder),
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	}
}

// HTTPHandlerSet contains all service http handlers to register it in mux.router later.
type HTTPHandlerSet struct {
	CreateAccountHandler       http.Handler
	ReadAccountHandler         http.Handler
	ListAccountHandler         http.Handler
	AuthenticateAccountHandler http.Handler
}

// NewHTTPHandlerSet returns a http handler set for registration in the mux.Router later
func NewHTTPHandlerSet(endpoints Endpoints, params HTTPHandlerParams) HTTPHandlerSet {
	return HTTPHandlerSet{
		CreateAccountHandler: makeCreateAccountHandler(
			endpoints,
			getHTTPHandlerOptions("Create", params),
		),
		ReadAccountHandler: makeReadAccountHandler(
			endpoints,
			getHTTPHandlerOptions("Read", params),
		),
		ListAccountHandler: makeListAccountHandler(
			endpoints,
			getHTTPHandlerOptions("List", params),
		),
		AuthenticateAccountHandler: makeAuthenticateAccountHandler(
			endpoints,
			getHTTPHandlerOptions("Authenticate", params),
		),
	}
}

// Register add handlers to mux.Router with theirs paths.
func (h *HTTPHandlerSet) Register(m *mux.Router) {
	m.Methods("POST").Path("/v0/accounts").Handler(h.CreateAccountHandler)
	m.Methods("GET").Path("/v0/accounts").Handler(h.ListAccountHandler)
	m.Methods("GET").Path("/v0/accounts/{id}").Handler(h.ReadAccountHandler)
	m.Methods("POST").Path("/v0/authenticate").Handler(h.AuthenticateAccountHandler)
}

func makeCreateAccountHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.CreateAccountEndpoint,
		decodeCreateAccountRequest,
		encodeResponse,
		options...,
	)
}

func makeReadAccountHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.ReadEndpoint,
		decodeReadAccountRequest,
		encodeResponse,
		options...,
	)
}

func makeListAccountHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.ListEndpoint,
		decodeListAccountRequest,
		encodeResponse,
		options...,
	)
}

func makeAuthenticateAccountHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.AuthenticateEndpoint,
		decodeAuthenticateRequest,
		encodeResponse,
		options...,
	)
}

func decodeCreateAccountRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req createAccountRequest
	if e := json.NewDecoder(r.Body).Decode(&req.CreateDTO); e != nil {
		return nil, pkgerrors.ErrInvalidBody
	}
	return req, nil
}

func decodeReadAccountRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	return readRequest{ID: id}, nil
}

func decodeListAccountRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	req := listRequest{}
	queryVals := r.URL.Query()
	req.Login = strings.TrimSpace(queryVals.Get("login"))
	limitStr := queryVals.Get("limit")

	if limitStr != "" {
		limit, err := strconv.Atoi(limitStr)
		if err != nil {
			return nil, pkgerrors.ErrInvalidQueryParameters
		}
		req.Limit = limit
	}

	offsetStr := queryVals.Get("offset")
	if offsetStr != "" {
		offset, err := strconv.Atoi(offsetStr)
		if err != nil {
			return nil, pkgerrors.ErrInvalidQueryParameters
		}
		req.Offset = int64(offset)
	}
	return req, nil
}

func decodeAuthenticateRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req authenticateRequest
	if e := json.NewDecoder(r.Body).Decode(&req); e != nil {
		return nil, pkgerrors.ErrInvalidBody
	}
	return req, nil
}

// encodeResponse is the common method to encode all response types to the
// client. I chose to do it this way because, since we're using JSON, there's no
// reason to provide anything more specific. It's certainly possible to
// specialize on a per-response (per-method) basis.
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(kitendpoint.Failer); ok && f.Failed() != nil {
		pkghttp.ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	if headerer, ok := response.(kithttp.Headerer); ok {
		for k, values := range headerer.Headers() {
			for _, v := range values {
				w.Header().Add(k, v)
			}
		}
	}
	code := http.StatusOK
	if sc, ok := response.(kithttp.StatusCoder); ok {
		code = sc.StatusCode()
	}
	w.WriteHeader(code)
	return kithttp.EncodeJSONResponse(ctx, w, response)
}
