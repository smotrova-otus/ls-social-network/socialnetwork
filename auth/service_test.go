package auth

import (
	"context"
	"errors"
	"github.com/stretchr/testify/require"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"testing"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	pkgerrors "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
)

func Test_service_Create(t *testing.T) {
	ctx, _ := log.Default()
	tokenServiceMock := newTokenServiceMock()
	accountRepositoryMock := newAccountRepositoryMock()
	passwordHasherMock := newPasswordHasherMock()
	uuidGeneratorMock := uuid.NewGeneratorMock()
	type args struct {
		ctx context.Context
		dto CreateDTO
	}
	tests := []struct {
		name       string
		args       args
		wantResult *domain.Account
		wantErr    error
		setup      func()
	}{
		{
			name: "success",
			setup: func() {
				accountRepositoryMock.setExists(
					"test@admin.ru",
					false,
					nil,
				)
				passwordHasherMock.setMakeSalt([]byte("some salt"), nil)
				passwordHasherMock.setCreateHash(
					[]byte("password"),
					[]byte("some salt"),
					[]byte("some hash"),
					nil,
				)
				accountRepositoryMock.setInsert(
					&domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					nil,
				)
				uuidGeneratorMock.SetNew(uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"))
			},
			args: args{
				ctx: ctx,
				dto: CreateDTO{
					Login:    "TEST@admin.ru",
					Password: " password ",
				},
			},
			wantResult: &domain.Account{
				ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
				Login:        "test@admin.ru",
				PasswordHash: []byte("some hash"),
				PasswordSalt: []byte("some salt"),
			},
			wantErr: nil,
		},
		{
			name: "account_already_exists",
			setup: func() {
				accountRepositoryMock.setExists(
					"test@admin.ru",
					true,
					nil,
				)
			},
			args: args{
				ctx: ctx,
				dto: CreateDTO{
					Login:    "test@admin.ru",
					Password: "password",
				},
			},
			wantResult: nil,
			wantErr:    errAccountAlreadyExists,
		},
		{
			name:  "validation_error",
			setup: func() {},
			args: args{
				ctx: ctx,
				dto: CreateDTO{
					Login:    "",
					Password: "password",
				},
			},
			wantResult: nil,
			wantErr:    errEmptyLogin,
		},
		{
			name: "exists_error",
			setup: func() {
				accountRepositoryMock.setExists(
					"test@admin.ru",
					false,
					errors.New("some db error"),
				)
			},
			args: args{
				ctx: ctx,
				dto: CreateDTO{
					Login:    "test@admin.ru",
					Password: "password",
				},
			},
			wantResult: nil,
			wantErr:    errors.New("some db error"),
		},
		{
			name: "make_salt_error",
			setup: func() {
				accountRepositoryMock.setExists(
					"test@admin.ru",
					false,
					nil,
				)
				uuidGeneratorMock.SetNew(uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"))
				passwordHasherMock.setMakeSalt([]byte("some salt"), errors.New("make salt error"))
			},
			args: args{
				ctx: ctx,
				dto: CreateDTO{
					Login:    "test@admin.ru",
					Password: "password",
				},
			},
			wantResult: nil,
			wantErr:    errors.New("make salt error"),
		},
		{
			name: "create_hash_error",
			setup: func() {
				accountRepositoryMock.setExists(
					"test@admin.ru",
					false,
					nil,
				)
				uuidGeneratorMock.SetNew(uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"))
				passwordHasherMock.setMakeSalt([]byte("some salt"), nil)
				passwordHasherMock.setCreateHash(
					[]byte("password"),
					[]byte("some salt"),
					[]byte("some hash"),
					errors.New("create hash error"),
				)
			},
			args: args{
				ctx: ctx,
				dto: CreateDTO{
					Login:    "test@admin.ru",
					Password: "password",
				},
			},
			wantResult: nil,
			wantErr:    errors.New("create hash error"),
		},
		{
			name: "save_error",
			setup: func() {
				accountRepositoryMock.setExists(
					"test@admin.ru",
					false,
					nil,
				)
				passwordHasherMock.setMakeSalt([]byte("some salt"), nil)
				passwordHasherMock.setCreateHash(
					[]byte("password"),
					[]byte("some salt"),
					[]byte("some hash"),
					nil,
				)
				accountRepositoryMock.setInsert(
					&domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					errors.New("some error"),
				)
				uuidGeneratorMock.SetNew(uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"))
			},
			args: args{
				ctx: ctx,
				dto: CreateDTO{
					Login:    "test@admin.ru",
					Password: "password",
				},
			},
			wantResult: nil,
			wantErr:    errors.New("some error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				tokenServiceMock.AssertExpectations(t)
				tokenServiceMock.ExpectedCalls = nil
				accountRepositoryMock.AssertExpectations(t)
				accountRepositoryMock.ExpectedCalls = nil
				passwordHasherMock.AssertExpectations(t)
				passwordHasherMock.ExpectedCalls = nil
				uuidGeneratorMock.AssertExpectations(t)
				uuidGeneratorMock.ExpectedCalls = nil
			}()
			s := NewService(
				tokenServiceMock,
				accountRepositoryMock,
				passwordHasherMock,
				uuidGeneratorMock,
			)
			gotResult, err := s.Create(tt.args.ctx, tt.args.dto)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_Authenticate(t *testing.T) {
	ctx, _ := log.Default()
	tokenServiceMock := newTokenServiceMock()
	accountRepositoryMock := newAccountRepositoryMock()
	passwordHasherMock := newPasswordHasherMock()
	uuidGeneratorMock := uuid.NewGeneratorMock()
	type args struct {
		ctx context.Context
		dto LoginDTO
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
		wantErr    error
		setup      func()
	}{
		{
			name: "success",
			setup: func() {
				accountRepositoryMock.setFindByLogin(
					"test@admin.ru",
					&domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					nil,
				)
				passwordHasherMock.setCreateHash(
					[]byte("password"),
					[]byte("some salt"),
					[]byte("some hash"),
					nil,
				)
				tokenServiceMock.setMakeToken(
					domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					"some token",
					nil,
				)
			},
			args: args{
				ctx: ctx,
				dto: LoginDTO{
					Login:    "TEST@admin.ru",
					Password: " password ",
				},
			},
			wantResult: "some token",
			wantErr:    nil,
		},
		{
			name: "find_by_login_error",
			setup: func() {
				accountRepositoryMock.setFindByLogin(
					"test@admin.ru",
					nil,
					errors.New("some error"),
				)

			},
			args: args{
				ctx: ctx,
				dto: LoginDTO{
					Login:    "TEST@admin.ru",
					Password: " password ",
				},
			},
			wantResult: "",
			wantErr:    errors.New("some error"),
		},
		{
			name: "login_not_foud",
			setup: func() {
				accountRepositoryMock.setFindByLogin(
					"test@admin.ru",
					nil,
					nil,
				)

			},
			args: args{
				ctx: ctx,
				dto: LoginDTO{
					Login:    "TEST@admin.ru",
					Password: " password ",
				},
			},
			wantResult: "",
			wantErr:    errBadCredentials,
		},
		{
			name: "bad_password",
			setup: func() {
				accountRepositoryMock.setFindByLogin(
					"test@admin.ru",
					&domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					nil,
				)
				passwordHasherMock.setCreateHash(
					[]byte("password"),
					[]byte("some salt"),
					[]byte("another hash"),
					nil,
				)
			},
			args: args{
				ctx: ctx,
				dto: LoginDTO{
					Login:    "TEST@admin.ru",
					Password: " password ",
				},
			},
			wantResult: "",
			wantErr:    errBadCredentials,
		},
		{
			name: "hash_create_error",
			setup: func() {
				accountRepositoryMock.setFindByLogin(
					"test@admin.ru",
					&domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					nil,
				)
				passwordHasherMock.setCreateHash(
					[]byte("password"),
					[]byte("some salt"),
					nil,
					errors.New("some error"),
				)
			},
			args: args{
				ctx: ctx,
				dto: LoginDTO{
					Login:    "TEST@admin.ru",
					Password: " password ",
				},
			},
			wantResult: "",
			wantErr:    errors.New("some error"),
		},
		{
			name: "token_create_error",
			setup: func() {
				accountRepositoryMock.setFindByLogin(
					"test@admin.ru",
					&domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					nil,
				)
				passwordHasherMock.setCreateHash(
					[]byte("password"),
					[]byte("some salt"),
					[]byte("some hash"),
					nil,
				)
				tokenServiceMock.setMakeToken(
					domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					"",
					errors.New("some error"),
				)
			},
			args: args{
				ctx: ctx,
				dto: LoginDTO{
					Login:    "TEST@admin.ru",
					Password: " password ",
				},
			},
			wantResult: "",
			wantErr:    errors.New("some error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				tokenServiceMock.AssertExpectations(t)
				tokenServiceMock.ExpectedCalls = nil
				accountRepositoryMock.AssertExpectations(t)
				accountRepositoryMock.ExpectedCalls = nil
				passwordHasherMock.AssertExpectations(t)
				passwordHasherMock.ExpectedCalls = nil
				uuidGeneratorMock.AssertExpectations(t)
				uuidGeneratorMock.ExpectedCalls = nil
			}()
			s := NewService(
				tokenServiceMock,
				accountRepositoryMock,
				passwordHasherMock,
				uuidGeneratorMock,
			)
			gotResult, err := s.Authenticate(tt.args.ctx, tt.args.dto)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_Read(t *testing.T) {
	ctx, _ := log.Default()
	accountRepositoryMock := newAccountRepositoryMock()
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name       string
		args       args
		wantResult *domain.Account
		wantErr    error
		setup      func()
	}{
		{
			name: "success",
			setup: func() {
				accountRepositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					&domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@admin.ru",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantResult: &domain.Account{
				ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
				Login:        "test@admin.ru",
				PasswordHash: []byte("some hash"),
				PasswordSalt: []byte("some salt"),
			},
			wantErr: nil,
		},
		{
			name: "not_found",
			setup: func() {
				accountRepositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantResult: nil,
			wantErr:    pkgerrors.ErrNotFound,
		},
		{
			name: "find_error",
			setup: func() {
				accountRepositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
					errors.New("some error"),
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantResult: nil,
			wantErr:    errors.New("some error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				accountRepositoryMock.AssertExpectations(t)
				accountRepositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				nil,
				accountRepositoryMock,
				nil,
				nil,
			)
			gotResult, err := s.Read(tt.args.ctx, tt.args.id)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_List(t *testing.T) {
	ctx, _ := log.Default()
	accountRepositoryMock := newAccountRepositoryMock()
	type args struct {
		ctx    context.Context
		filter filter
		offset int64
		limit  int
	}
	tests := []struct {
		name       string
		args       args
		wantResult []domain.Account
		wantErr    error
		setup      func()
	}{
		{
			name: "success",
			setup: func() {
				accountRepositoryMock.setFindAllWithFilter(
					0,
					10,
					filter{
						Login: "test@gmail.com",
					},
					[]domain.Account{
						{
							ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
							Login:        "test@admin.ru",
							PasswordHash: []byte("some hash"),
							PasswordSalt: []byte("some salt"),
						},
					},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				filter: filter{
					Login: "test@gmail.com",
				},
				offset: 0,
				limit:  10,
			},
			wantResult: []domain.Account{
				{
					ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					Login:        "test@admin.ru",
					PasswordHash: []byte("some hash"),
					PasswordSalt: []byte("some salt"),
				},
			},
			wantErr: nil,
		},
		{
			name: "empty",
			setup: func() {
				accountRepositoryMock.setFindAllWithFilter(
					0,
					10,
					filter{
						Login: "test@gmail.com",
					},
					[]domain.Account{},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				filter: filter{
					Login: "test@gmail.com",
				},
				offset: 0,
				limit:  10,
			},
			wantResult: []domain.Account{},
			wantErr:    nil,
		},
		{
			name: "find_error",
			setup: func() {
				accountRepositoryMock.setFindAllWithFilter(
					0,
					10,
					filter{
						Login: "test@gmail.com",
					},
					[]domain.Account{},
					errors.New("some error"),
				)
			},
			args: args{
				ctx: ctx,
				filter: filter{
					Login: "test@gmail.com",
				},
				offset: 0,
				limit:  10,
			},
			wantResult: nil,
			wantErr:    errors.New("some error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				accountRepositoryMock.AssertExpectations(t)
				accountRepositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				nil,
				accountRepositoryMock,
				nil,
				nil,
			)
			gotResult, err := s.List(tt.args.ctx, tt.args.limit, tt.args.offset, tt.args.filter)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}
