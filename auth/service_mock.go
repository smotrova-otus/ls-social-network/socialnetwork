package auth

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type serviceMock struct {
	mock.Mock
}

func newServiceMock() *serviceMock {
	return &serviceMock{}
}

// Create implementation.
func (m *serviceMock) Create(ctx context.Context, dto CreateDTO) (result *domain.Account, err error) {
	results := m.Called(ctx, dto)
	return results.Get(0).(*domain.Account), results.Error(1)
}

// Authenticate implementation.
func (m *serviceMock) Authenticate(ctx context.Context, dto LoginDTO) (token string, err error) {
	results := m.Called(ctx, dto)
	return results.Get(0).(string), results.Error(1)
}

// Read implementation.
func (m *serviceMock) Read(ctx context.Context, id uuid.UUID) (result *domain.Account, err error) {
	results := m.Called(ctx, id)
	return results.Get(0).(*domain.Account), results.Error(1)
}

// List implementation.
func (m *serviceMock) List(ctx context.Context, limit int, offset int64, filter filter) ([]domain.Account, error) {
	results := m.Called(ctx, limit, offset, filter)
	return results.Get(0).([]domain.Account), results.Error(1)
}

// setCreate sets mock for method Create.
func (m *serviceMock) setCreate(dto CreateDTO, acc *domain.Account, err error) {
	m.On(
		"Create",
		mock.Anything,
		dto,
	).Return(acc, err).Once()
}

// setAuthenticate sets mock for method Authenticate.
func (m *serviceMock) setAuthenticate(dto LoginDTO, token string, err error) {
	m.On(
		"Authenticate",
		mock.Anything,
		dto,
	).Return(token, err).Once()
}

// setRead sets mock for method Read.
func (m *serviceMock) setRead(id uuid.UUID, acc *domain.Account, err error) {
	m.On(
		"Read",
		mock.Anything,
		id,
	).Return(acc, err).Once()
}

// setList sets mock for method List.
func (m *serviceMock) setList(limit int, offset int64, f filter, accounts []domain.Account, err error) {
	m.On(
		"List",
		mock.Anything,
		limit,
		offset,
		f,
	).Return(accounts, err).Once()
}
