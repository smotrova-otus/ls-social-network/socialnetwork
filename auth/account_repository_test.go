package auth

import (
	"context"
	"errors"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"os"
	"testing"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/database"
)

var db *sqlx.DB

func Test_accountRepository_Exists(t *testing.T) {
	type args struct {
		login string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr error
		setup   func(execer database.QueryerExecer)
	}{
		{
			name: "not_exists",
			args: args{
				login: "some@admin.ru",
			},
			want:    false,
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {

			},
		},
		{
			name: "exists",
			args: args{
				login: "test@admin.ru",
			},
			want:    true,
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				// create account
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test@admin.ru', 'x7124B37CCA7E7F3BC245C47A1215901BFB02A2C218DFC3D3733E2054216477FA', 'xD192AE28FAF6438907647F66B1D15D6628C4FC05D00E4D9B51F03DFCBC7C48B3');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewAccountRepository(tx)
			got, err := r.Exists(tt.args.login)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.want, got)
		})
	}
}

func Test_accountRepository_Exists_Error(t *testing.T) {
	queryerExecerMock := database.NewQueryerExecerMock()
	type args struct {
		login string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr error
		setup   func()
	}{
		{
			name: "error",
			args: args{
				login: "some@admin.ru",
			},
			want:    false,
			wantErr: errors.New("connection error"),
			setup: func() {
				queryerExecerMock.On(
					"QueryRowx",
					mock.Anything,
					mock.Anything,
					mock.Anything,
				).
					Return(database.NewSqlxRowWithError(errors.New("connection error")))
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			r := NewAccountRepository(queryerExecerMock)
			got, err := r.Exists(tt.args.login)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.want, got)
			queryerExecerMock.AssertExpectations(t)
			queryerExecerMock.ExpectedCalls = nil
		})
	}
}

func Test_accountRepository_FindByLogin(t *testing.T) {
	type args struct {
		login string
	}
	tests := []struct {
		name        string
		args        args
		wantAccount *domain.Account
		wantErr     error
		setup       func(execer database.QueryerExecer)
	}{
		{
			name: "exists",
			args: args{
				login: "test@admin.ru",
			},
			wantAccount: &domain.Account{
				ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				Login:        "test@admin.ru",
				PasswordHash: []byte("x7124B37CCA7E"),
				PasswordSalt: []byte("x7124B37CCA7EF"),
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test@admin.ru', 'x7124B37CCA7E', 'x7124B37CCA7EF');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
		{
			name: "not_exists",
			args: args{
				login: "test1@admin.ru",
			},
			wantAccount: nil,
			wantErr:     nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test@admin.ru', 'x7124B37CCA7E', 'x7124B37CCA7EF');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewAccountRepository(tx)
			gotAccount, err := r.FindByLogin(tt.args.login)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.wantAccount, gotAccount)
		})
	}
}

func Test_accountRepository_FindByID(t *testing.T) {
	type args struct {
		id uuid.UUID
	}
	tests := []struct {
		name        string
		args        args
		wantAccount *domain.Account
		wantErr     error
		setup       func(execer database.QueryerExecer)
	}{
		{
			name: "exists",
			args: args{
				id: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
			},
			wantAccount: &domain.Account{
				ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				Login:        "test@admin.ru",
				PasswordHash: []byte("x7124B37CCA7E"),
				PasswordSalt: []byte("x7124B37CCA7EF"),
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test@admin.ru', 'x7124B37CCA7E', 'x7124B37CCA7EF');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
		{
			name: "not_exists",
			args: args{
				id: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
			},
			wantAccount: nil,
			wantErr:     nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test@admin.ru', 'x7124B37CCA7E', 'x7124B37CCA7EF');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewAccountRepository(tx)
			gotAccount, err := r.FindByID(tt.args.id)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.wantAccount, gotAccount)
		})
	}
}

func Test_accountRepository_Insert(t *testing.T) {
	type args struct {
		account *domain.Account
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		setup   func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				account: &domain.Account{
					ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
					Login:        "test@admin.ru",
					PasswordHash: []byte("hash"),
					PasswordSalt: []byte("salt"),
				},
			},
			wantErr: false,
			setup: func(execer database.QueryerExecer) {

			},
		},
		{
			name: "account_already_exists",
			args: args{
				account: &domain.Account{
					ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
					Login:        "test@admin.ru",
					PasswordHash: []byte("hash"),
					PasswordSalt: []byte("salt"),
				},
			},
			wantErr: true,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test@admin.ru', 'x7124B37CCA7E', 'x7124B37CCA7EF');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewAccountRepository(tx)
			err = r.Insert(tt.args.account)
			require.Equal(t, tt.wantErr, err != nil)
			if err != nil {
				return
			}

			account := &domain.Account{}
			err = sqlx.Get(r.db, account, "select * from accounts limit 1")

			require.NoError(t, err)
			require.Equal(t, account, tt.args.account)
		})
	}
}

func Test_accountRepository_Update(t *testing.T) {
	type args struct {
		account *domain.Account
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
		setup   func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				account: &domain.Account{
					ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
					Login:        "test@admin.ru1",
					PasswordHash: []byte("hash1"),
					PasswordSalt: []byte("salt1"),
				},
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac2', 'test@admin.ru', 'x7124B37CCA7E', 'x7124B37CCA7EF');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
		{
			name: "account_not_exists",
			args: args{
				account: &domain.Account{
					ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
					Login:        "test@admin.ru1",
					PasswordHash: []byte("hash1"),
					PasswordSalt: []byte("salt1"),
				},
			},
			wantErr: database.ErrNoFound,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', 'test@admin.ru', 'x7124B37CCA7E', 'x7124B37CCA7EF');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewAccountRepository(tx)
			err = r.Update(tt.args.account)
			require.Equal(t, tt.wantErr, err)
			if err != nil {
				return
			}

			account := &domain.Account{}
			err = sqlx.Get(r.db, account, "select * from accounts limit 1")

			require.NoError(t, err)
			require.Equal(t, account, tt.args.account)
		})
	}
}

func Test_accountRepository_FindAllWithFilter(t *testing.T) {
	type args struct {
		filter filter
	}
	tests := []struct {
		name         string
		args         args
		wantAccounts []domain.Account
		wantErr      error
		setup        func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				filter: filter{
					Login: "test@admin.ru",
				},
			},
			wantAccounts: []domain.Account{
				{
					ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					Login:        "test@admin.ru",
					PasswordHash: []byte("x7124B37CCA7E"),
					PasswordSalt: []byte("x7124B37CCA7EF"),
				},
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO accounts (id, login, password_hash, password_salt)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test@admin.ru', 'x7124B37CCA7E', 'x7124B37CCA7EF');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewAccountRepository(tx)
			gotAccounts, err := r.FindAllWithFilter(0, -1, tt.args.filter)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.wantAccounts, gotAccounts)
		})
	}
}

func init() {
	dsn := os.Getenv("DSN")
	if dsn == "" {
		panic("require set end DSN")
	}
	var err error
	db, err = sqlx.Connect("mysql", dsn)
	if err != nil {
		panic(err)
	}
}
