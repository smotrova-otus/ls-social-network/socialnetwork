package auth

import (
	"context"
	"errors"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	pkgerrors "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

// TestRouter tests that all routes are bound to handlers.
func TestRouter(t *testing.T) {
	var ep = func(
		ctx context.Context,
		request interface{},
	) (response interface{}, err error) {
		return nil, nil
	}

	_, logger := log.Default()
	tests := []struct {
		name   string
		method string
		url    string
	}{
		{
			name:   "POST_/v0/accounts",
			url:    "/v0/accounts",
			method: http.MethodPost,
		},
		{
			name:   "GET_/v0/accounts/7d2838fc-cbf8-4553-a671-474c591bcac3",
			url:    "/v0/accounts/7d2838fc-cbf8-4553-a671-474c591bcac3",
			method: http.MethodGet,
		},
		{
			name:   "POST_/v0/authenticate",
			url:    "/v0/authenticate",
			method: http.MethodPost,
		},
		{
			name:   "GET_/v0/accounts",
			url:    "/v0/accounts",
			method: http.MethodGet,
		},
	}
	router := mux.NewRouter()

	handlerSet := NewHTTPHandlerSet(
		Endpoints{
			CreateAccountEndpoint: ep,
			AuthenticateEndpoint:  ep,
			ReadEndpoint:          ep,
			ListEndpoint:          ep,
		},
		HTTPHandlerParams{
			Logger: logger,
		},
	)
	// All checked handlers must be added to router here.
	handlerSet.Register(router)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res := httptest.NewRecorder()
			req, _ := http.NewRequest(
				tt.method,
				tt.url,
				strings.NewReader("{}"),
			)
			router.ServeHTTP(res, req)

			assert.Equal(t, http.StatusOK, res.Code)
		})
	}
}

func TestMakeCreateAccountHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeCreateAccountHandler(Endpoints{
		CreateAccountEndpoint: MakeCreateAccountEndpoint(serviceMock),
	}, getHTTPHandlerOptions("Create", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url  string
		body string
	}

	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			args: args{
				url:  "/api/accounts",
				body: `{"login":"okLogin","password":"okPassword"}`,
			},
			setup: func() {
				serviceMock.setCreate(
					CreateDTO{
						Login:    "okLogin",
						Password: "okPassword",
					},
					&domain.Account{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "okLogin",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
					nil,
				)
			},
			wantCode: http.StatusOK,
			wantBody: `{"id":"7d2838fc-cbf8-4553-a671-474c591bcac3","login":"okLogin"}` + "\n",
		},
		{
			name: "service_error",
			args: args{
				url:  "/api/accounts",
				body: `{"login":"okLogin","password":"okPassword"}`,
			},
			setup: func() {
				serviceMock.setCreate(
					CreateDTO{
						Login:    "okLogin",
						Password: "okPassword",
					},
					nil,
					errors.New("some error"),
				)
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
		{
			name: "invalid body error",
			args: args{
				url:  "/api/accounts",
				body: `{"login":`,
			},
			setup:    func() {},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"request body is invalid","code":"INPUT_DATA_INVALID_BODY"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()

			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodPost,
				tt.args.url,
				strings.NewReader(tt.args.body),
			)
			if err != nil {
				panic(err)
			}

			handler.ServeHTTP(res, req)

			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code)
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestAuthenticateHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeAuthenticateAccountHandler(Endpoints{
		AuthenticateEndpoint: MakeAuthenticateEndpoint(serviceMock),
	}, getHTTPHandlerOptions("Authenticate", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url  string
		body string
	}

	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			args: args{
				url:  "/v0/authenticate",
				body: `{"login":"okLogin","password":"okPassword"}`,
			},
			setup: func() {
				serviceMock.setAuthenticate(
					LoginDTO{
						Login:    "okLogin",
						Password: "okPassword",
					},
					"some token",
					nil,
				)
			},
			wantCode: http.StatusOK,
			wantBody: `{"token":"some token"}` + "\n",
		},
		{
			name: "service_error",
			args: args{
				url:  "/v0/authenticate",
				body: `{"login":"okLogin","password":"okPassword"}`,
			},
			setup: func() {
				serviceMock.setAuthenticate(
					LoginDTO{
						Login:    "okLogin",
						Password: "okPassword",
					},
					"",
					errors.New("some error"),
				)
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
		{
			name: "invalid body error",
			args: args{
				url:  "/v0/authenticate",
				body: `{"login":`,
			},
			setup:    func() {},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"request body is invalid","code":"INPUT_DATA_INVALID_BODY"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()

			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodPost,
				tt.args.url,
				strings.NewReader(tt.args.body),
			)
			if err != nil {
				panic(err)
			}

			handler.ServeHTTP(res, req)

			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code)
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestReadHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeReadAccountHandler(Endpoints{
		ReadEndpoint: MakeReadEndpoint(serviceMock),
	}, getHTTPHandlerOptions("Read", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url     string
		muxVars map[string]string
	}

	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setRead(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					&domain.Account{
						ID:    uuid.MustParse("00000000-0000-4000-8000-000000000000"),
						Login: "okLogin",
					},
					nil,
				)
			},
			args: args{
				url:     "/v0/accounts/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
			},
			wantCode: http.StatusOK,
			wantBody: `{"id":"00000000-0000-4000-8000-000000000000","login":"okLogin"}` + "\n",
		},
		{
			name:  "invalid account id",
			setup: func() {},
			args: args{
				url:     "/v0/accounts/invalid_uuid",
				muxVars: map[string]string{"id": "invalid_uuid"},
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid path parameters","code":"INVALID_PATH_PARAMETERS"}`,
		},
		{
			name: "account not found err",
			setup: func() {
				serviceMock.setRead(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					nil,
					pkgerrors.ErrNotFound,
				)
			},
			args: args{
				url:     "/v0/accounts/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
			},
			wantCode: http.StatusNotFound,
			wantBody: `{"error":"not found","code":"NOT_FOUND"}`,
		},
		{
			name: "general service err",
			setup: func() {
				serviceMock.setRead(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					nil,
					errors.New("general error"),
				)
			},
			args: args{
				url:     "/v0/accounts/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodGet,
				tt.args.url,
				nil,
			)
			if err != nil {
				t.Fatal(err)
			}
			req = mux.SetURLVars(req, tt.args.muxVars)
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestListHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeListAccountHandler(Endpoints{
		ListEndpoint: MakeListEndpoint(serviceMock),
	}, getHTTPHandlerOptions("List", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url string
	}
	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setList(1, 1, filter{
					"test@test.ts",
				}, []domain.Account{
					{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@test.ts",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
				}, nil)
			},
			args: args{
				url: "/v0/accounts?login=test@test.ts&limit=1&offset=1",
			},
			wantCode: http.StatusOK,
			wantBody: `{"data":[{"id":"7d2838fc-cbf8-4553-a671-474c591bcac3","login":"test@test.ts"}]}` + "\n",
		},
		{
			name: "success_without_login_and_offset",
			setup: func() {
				serviceMock.setList(0, 0, filter{
					Login: "",
				}, []domain.Account{
					{
						ID:           uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						Login:        "test@test.ts",
						PasswordHash: []byte("some hash"),
						PasswordSalt: []byte("some salt"),
					},
				}, nil)
			},
			args: args{
				url: "/v0/accounts",
			},
			wantCode: http.StatusOK,
			wantBody: `{"data":[{"id":"7d2838fc-cbf8-4553-a671-474c591bcac3","login":"test@test.ts"}]}` + "\n",
		},
		{
			name: "service_fail",
			setup: func() {
				serviceMock.setList(1, 1, filter{}, []domain.Account{}, errors.New("some err"))
			},
			args: args{
				url: "/v0/accounts?limit=1&offset=1",
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
		{
			name: "bad_limit",
			setup: func() {
			},
			args: args{
				url: "/v0/accounts?limit=asd",
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid query parameters","code":"INVALID_QUERY_PARAMETERS"}`,
		},
		{
			name: "bad_offset",
			setup: func() {
			},
			args: args{
				url: "/v0/accounts?offset=asd",
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid query parameters","code":"INVALID_QUERY_PARAMETERS"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodGet,
				tt.args.url,
				nil,
			)
			if err != nil {
				t.Fatal(err)
			}
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}
