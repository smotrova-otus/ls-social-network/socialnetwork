package auth

import "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"

func convertFromCreateDto(dto CreateDTO) domain.Account {
	return domain.Account{
		Login: dto.Login,
	}
}
