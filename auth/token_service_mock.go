package auth

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
)

// tokenService is a mock implementation of TokenValidator and TokenCreator interface.
type tokenServiceMock struct {
	mock.Mock
}

// newTokenServiceMock is a constructor.
func newTokenServiceMock() *tokenServiceMock {
	return &tokenServiceMock{}
}

// MakeToken implementation.
func (m *tokenServiceMock) MakeToken(acc domain.Account) (string, error) {
	results := m.Called(acc)
	return results.Get(0).(string), results.Error(1)
}

// setMakeToken set mock for method MakeToken.
func (m *tokenServiceMock) setMakeToken(acc domain.Account, token string, err error) {
	m.On("MakeToken", acc).
		Return(token, err)
}
