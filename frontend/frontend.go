package frontend

import (
	"github.com/gobuffalo/packr/v2"
	"github.com/gorilla/mux"
	"net/http"
)

var box = packr.New("build", "build")

func Register(router *mux.Router) {
	prefix := "/ui/"
	router.PathPrefix(prefix).Handler(http.StripPrefix(prefix, http.FileServer(&filesystem{box})))
}

type filesystem struct {
	*packr.Box
}

func (f *filesystem) Open(name string) (http.File, error) {
	file, err := f.Box.Open(name)
	if err != nil {
		return f.Box.Open("/index.html")
	}
	return file, nil
}
