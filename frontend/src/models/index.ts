export interface Account {
    id: string
    login: string
    password?: string
}

export interface Profile {
    id: string
    first_name: string
    last_name: string
    city?: string
    birth_date?: string
    interests?: string
    gender?: "male" | "female"
}

export type ProfileWithFollowing = Profile & {
    isFollowing: boolean
}

export const displayProfileName = (profile: Profile) => `${profile.first_name} ${profile.last_name}`
