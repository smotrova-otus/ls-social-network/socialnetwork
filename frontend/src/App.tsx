import 'date-fns'
import React, {useMemo} from 'react'
import {AuthSignUp} from "./pages/AuthSignUp/AuthSignUp"
import {ConfigContext, localStorageTokenProvider} from "./hooks/api"
import {AuthSignIn} from "./pages/AuthSignIn/AuthSignIn"
import {createMuiTheme, ThemeProvider} from "@material-ui/core/styles"
import {pink, blue, indigo} from "@material-ui/core/colors"
import CssBaseline from "@material-ui/core/CssBaseline"
import useMediaQuery from "@material-ui/core/useMediaQuery"
import {SnackbarProvider} from "notistack"
import {Switch, Route, BrowserRouter} from "react-router-dom"
import {Profile} from "./pages/Profile/Profile"
import {Friends} from "./pages/Friends/Friends"
import DateFnsUtils from "@date-io/date-fns"
import {MuiPickersUtilsProvider} from "@material-ui/pickers"
import {BrowsePeople} from "./pages/BrowsePeople/BrowsePeople";

function App() {
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')

    const theme = useMemo(
        () => {
            const palletType = prefersDarkMode ? "dark" : "light"
            const mainPrimaryColor = prefersDarkMode ? blue[200] : indigo[500]
            const mainSecondaryColor = prefersDarkMode ? pink[200] : pink["A400"]
            return createMuiTheme({
                palette: {
                    type: palletType,
                    primary: {
                        main: mainPrimaryColor
                    },
                    secondary: {
                        main: mainSecondaryColor
                    }
                },
            })
        },
        [prefersDarkMode],
    )

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <ConfigContext.Provider value={{
                baseURL: process.env.REACT_APP_API_URL || '/v0/',
                getToken: localStorageTokenProvider
            }}>
                <ThemeProvider theme={theme}>
                    <SnackbarProvider maxSnack={3}>
                        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                        <CssBaseline/>
                        <BrowserRouter basename={process.env.PUBLIC_URL}>
                            <Switch>
                                <Route exact path="/">
                                    <Profile/>
                                </Route>
                                <Route exact path="/profile/:id">
                                    <Profile/>
                                </Route>
                                <Route exact path="/signUp">
                                    <AuthSignUp/>
                                </Route>
                                <Route exact path="/signIn">
                                    <AuthSignIn/>
                                </Route>
                                <Route exact path="/userFriends">
                                    <Friends/>
                                </Route>
                                <Route exact path="/browse">
                                    <BrowsePeople/>
                                </Route>
                            </Switch>
                        </BrowserRouter>
                    </SnackbarProvider>
                </ThemeProvider>
            </ConfigContext.Provider>
        </MuiPickersUtilsProvider>
    )
}

export default App
