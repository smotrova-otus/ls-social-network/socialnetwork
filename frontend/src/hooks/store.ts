import create from "zustand"
import * as models from "../models"
import reduce from "lodash/reduce"
import pickBy from "lodash/pickBy"
import {createSelector} from "reselect"
import map from "lodash/map"
import find from "lodash/find"

export type State = {
    accounts: Record<string, models.Account>
    profiles: Record<string, models.Profile>
    followings: Record<string, models.Profile>
    followers: Record<string, models.Profile>

    putProfile: (profile: models.Profile) => void
    setProfiles: (profiles: models.Profile[]) => void
    putProfiles: (profiles: models.Profile[]) => void
    setFollowers: (profiles: models.Profile[]) => void
    setFollowings: (profiles: models.Profile[]) => void
    putFollowing: (props: models.Profile) => void
    deleteFollowing: (props: { followingId: string }) => void
}

const reduceProfiles = (result: Record<string, models.Profile>, value: models.Profile) => {
    result[value.id] = value
    return result
}

export const useStore = create<State>(set => ({
    accounts: {},
    profiles: {},
    followings: {},
    followers: {},

    putProfile: (profile: models.Profile) => set(state => ({
        ...state,
        profiles: {
            ...state.profiles,
            [profile.id]: profile
        }
    })),
    setProfiles: (profiles: models.Profile[]) => set(state => ({
        ...state,
        profiles: {
            ...reduce(profiles, reduceProfiles, {}),
        }
    })),
    putProfiles: (profiles: models.Profile[]) => set(state => ({
        ...state,
        profiles: {
            ...state.profiles,
            ...reduce(profiles, reduceProfiles, {}),
        }
    })),
    setFollowers: (profiles: models.Profile[]) => set(state => ({
        ...state,
        followers: {
            ...reduce(profiles, reduceProfiles, {}),
        }
    })),
    setFollowings: (profiles: models.Profile[]) => set(state => ({
        ...state,
        followings: {
            ...reduce(profiles, reduceProfiles, {}),
        }
    })),
    putFollowing: (profile: models.Profile) => set(state => ({
        ...state,
        followings: {
            ...state.followings,
            [profile.id]: profile
        }
    })),
    deleteFollowing: (props: { followingId: string }) => set(state => ({
        ...state,
        followings: pickBy(state.followings, profile => profile.id !== props.followingId)
    }))
}))

export const selectProfile = (id?: string) => (state: State) => {
    if (!id) {
        return undefined
    }
    return state.profiles[id]
}

export const selectProfiles = (state: { profiles: Record<string, models.Profile> }) => state.profiles

export const createSelectProfilesWithoutMe = (selfId?: string) => {
    return createSelector(
        selectProfiles,
        profiles => pickBy(profiles, profile => profile.id !== selfId)
    )
}

export const selectFollowings = (state: { followings: Record<string, models.Profile> }) => state.followings

export const createSelectProfilesWithFollowStatus = (selfId?: string) => {
    return createSelector(
        createSelectProfilesWithoutMe(selfId),
        selectFollowings,
        (
            profiles,
            followings,
        ) => map(profiles, profile => ({
            ...profile,
            isFollowing: !!find(followings, following => profile.id === following.id)
        }))
    )
}

export const selectProfilesWithFollowStatus = createSelector(
    selectProfiles,
    selectFollowings,
    (
        profiles,
        followings,
    ) => map(profiles, profile => ({
        ...profile,
        isFollowing: !!find(followings, following => profile.id === following.id)
    }))
)

export const selectFriends = createSelector(
    selectProfiles,
    selectFollowings,
    (
        profiles,
        followings,
    ) => pickBy(profiles, profile => !!find(followings, following => profile.id === following.id))
)
