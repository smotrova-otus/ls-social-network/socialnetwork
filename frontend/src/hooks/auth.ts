import {useLocalStorage} from "./localstorage"
import jwtDecode from 'jwt-decode'
import {useCallback, useEffect, useMemo} from "react"
import {useNavigation} from "./navigation"

export interface Claims {
    sub: string
    subId: string
    exp: number
}

export const useToken = () => {
    return useLocalStorage("token", null)
}

export const useClaims = (): undefined | Claims => {
    const [token, setToken] = useToken()
    const {navigateSignIn} = useNavigation()

    const claims: Claims | undefined = useMemo(() => {
        try {
            return jwtDecode(token)
        } catch (e) {
            return undefined
        }
    }, [token])

    useEffect(() => {
        function checkToken() {
            if (token && (!claims || claims.exp < Date.now() / 1000)) {
                setToken('')
                navigateSignIn()
            }
        }

        const interval = setInterval(checkToken, 1000)
        return () => clearInterval(interval)
    }, [token, claims, navigateSignIn, setToken])

    return claims;
}

export const useLogout = () => {
    const [, setToken] = useToken()
    const {navigateSignIn} = useNavigation()

    return useCallback(() => {
        setToken("")
        navigateSignIn()
    }, [navigateSignIn, setToken])
}

export const useRequiredAuth = () => {
    const claims = useClaims()
    const {navigateSignIn} = useNavigation()

    if (!claims) {
        navigateSignIn()
    }
}

export const useRequiredNotAuth = () => {
    const {navigateHome} = useNavigation()
    const claims = useClaims()

    if (claims) {
        navigateHome()
    }
}
