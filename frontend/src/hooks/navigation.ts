import {useHistory} from "react-router-dom"
import {useMemo} from "react"

export const useNavigation = () => {
    const history = useHistory()

    return useMemo(() => ({
        navigateSignIn() {
            history.push("/signIn")
        },
        navigateSignUp() {
            history.push("/signUp")
        },
        navigateHome() {
            history.push("/")
        }
    }), [history])
}
