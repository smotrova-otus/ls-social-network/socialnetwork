import {useCallback, useState} from "react";
import useInfiniteScroll from "react-infinite-scroll-hook";

export const usePaging = (limit: number) => {
    const [loading, setLoading] = useState(false)
    const [hasNextPage, setHasNextPage] = useState(true)
    const [offset, setOffset] = useState(0)

    const onLoadMore = useCallback(() => {
        setOffset(offset + limit)
    }, [setOffset, offset, limit])

    const infiniteRef = useInfiniteScroll({
        loading,
        hasNextPage,
        onLoadMore,
    });

    const reset = useCallback(() => {
        setHasNextPage(true)
        setLoading(false)
        setOffset(0)
    }, [])

    return {
        setLoading,
        offset,
        setHasNextPage,
        reset,
        infiniteRef,
    }
}
