import {useApi} from "./api"
import {useSnackbar} from "notistack"
import * as models from "../models"
import {
    createSelectProfilesWithFollowStatus,
    selectFollowings,
    selectProfile,
    selectProfilesWithFollowStatus,
    useStore
} from "./store"
import {useNavigation} from "./navigation"
import {useCallback, useEffect, useMemo, useState} from "react"
import {useClaims, useRequiredAuth, useRequiredNotAuth, useToken} from "./auth"
import {useFormik} from "formik"
import {formatServerDate} from "../lib/date"
import values from "lodash/values"
import {useFilterStore} from "./filter";
import {usePaging} from "./paging";
import size from "lodash/size";

export const useLoginForm = () => {
    useRequiredNotAuth()

    const [, storeToken] = useToken()
    const {enqueueSnackbar} = useSnackbar()
    const {navigateHome} = useNavigation()
    const {unwrap, authenticate} = useApi()

    const login = useCallback(async (props: { login: string, password: string }) => {
        try {
            const {token} = await unwrap(authenticate(props))
            storeToken(token)
            navigateHome()
        } catch (error) {
            enqueueSnackbar(`Authenticate error: ${error.message}`, {variant: "error"})
        }
    }, [authenticate, enqueueSnackbar, navigateHome, storeToken, unwrap])

    return useFormik({
        initialValues: {
            login: '',
            password: ''
        },
        onSubmit: login,
    })
}

export const useRegisterForm = () => {
    useRequiredNotAuth()

    const [, storeToken] = useToken()
    const {enqueueSnackbar} = useSnackbar()
    const {navigateHome} = useNavigation()
    const {unwrap, postAccount, putProfile, authenticate} = useApi()

    type Fields = {
        login: string
        password: string
        first_name: string
        last_name: string
        gender: 'male' | 'female'
    }

    const register = useCallback(async (props: Fields) => {
        try {
            const {
                login,
                password,
                first_name,
                last_name,
                gender,
            } = props

            const {id} = await unwrap(postAccount({login, password}))
            const {token} = await unwrap(authenticate({
                login, password
            }))

            storeToken(token)

            await unwrap(putProfile({
                id,
                first_name,
                last_name,
                gender,
            }))

            navigateHome()
        } catch (error) {
            enqueueSnackbar(`Register error: ${error.message}`, {variant: "error"})
        }
    }, [authenticate, enqueueSnackbar, navigateHome, postAccount, putProfile, storeToken, unwrap])

    return useFormik<Fields>({
        initialValues: {
            login: '',
            password: '',
            first_name: '',
            last_name: '',
            gender: 'male',
        },
        onSubmit: register,
    })
}

export const useProfile = (id?: string) => {
    useRequiredAuth()

    const {enqueueSnackbar} = useSnackbar()
    const claims = useClaims()
    const {unwrap, getProfile} = useApi()
    const storeProfile = useStore(useCallback(state => state.putProfile, []))
    const profileId = id || claims?.subId
    const isCanEditProfile = profileId === claims?.subId

    useEffect(() => {
        (async function () {
            if (!profileId) return
            try {
                const profile = await unwrap(getProfile({id: profileId}))
                storeProfile(profile)
            } catch (error) {
                enqueueSnackbar(`Get user error: ${error.message}`, {variant: "error"})
            }
        }())
    }, [enqueueSnackbar, getProfile, storeProfile, unwrap, profileId])

    return {
        profile: useStore(useCallback((selectProfile(profileId)), [profileId])),
        isCanEditProfile,
    }
}

export const useProfileForm = (props: {
    profile: models.Profile,
    onSuccessCallback: (profile: models.Profile) => void
}) => {
    useRequiredAuth()

    const {onSuccessCallback, profile} = props
    const {enqueueSnackbar} = useSnackbar()
    const {unwrap, putProfile} = useApi()
    const storeProfile = useStore(useCallback(state => state.putProfile, []))

    const updateProfile = useCallback(async (profile: models.Profile) => {
        try {
            profile.birth_date = formatServerDate(profile.birth_date)

            await unwrap(putProfile(profile))
            storeProfile(profile)
        } catch (error) {
            enqueueSnackbar(`Update profile error: ${error.message}`, {variant: "error"})
            throw error
        }
    }, [enqueueSnackbar, putProfile, storeProfile, unwrap])

    const onSubmit = useCallback(async (profile: models.Profile) => {
        await updateProfile(profile)
        onSuccessCallback(profile)
    }, [onSuccessCallback, updateProfile])

    return useFormik({
        initialValues: profile,
        onSubmit,
    })
}

export const useBrowsePeople = (limit: number) => {
    const {
        offset,
        infiniteRef,
        setHasNextPage,
        reset: resetPager,
        setLoading,
    } = usePaging(limit)
    const claims = useClaims()
    const {enqueueSnackbar} = useSnackbar()
    const {unwrap, getProfiles, getProfileFollowings} = useApi()
    const setStoreProfiles = useStore(useCallback(state => state.setProfiles, []))
    const addProfilesToStore = useStore(useCallback(state => state.putProfiles, []))
    const storeFollowings = useStore(useCallback(state => state.setFollowings, []))
    const filterStore = useFilterStore()
    const [needToLoad, setNeedToLoad] = useState(true)

    useEffect(() => {
        setNeedToLoad(true)
    }, [offset])

    useEffect(() => {
        resetPager()
        setStoreProfiles([])
        setNeedToLoad(true)
    }, [setStoreProfiles, resetPager, filterStore.firstName, filterStore.lastName])

    useEffect(() => {
        (async function () {
            if (!claims) return

            try {
                const followings = await unwrap(getProfileFollowings({
                    id: claims.subId
                }))
                storeFollowings(followings.data)
            } catch (error) {
                enqueueSnackbar(`Fetch followings error: ${error.message}`, {variant: "error"})
            }
        }())
    }, [claims, enqueueSnackbar, getProfileFollowings, storeFollowings, unwrap])

    useEffect(() => {
        (async function () {
            if (!claims) return
            if (!needToLoad) return

            setNeedToLoad(false)
            setLoading(true)

            try {
                const profiles = await unwrap(getProfiles({
                    firstName: filterStore.firstName,
                    lastName: filterStore.lastName,
                    limit,
                    offset,
                }))

                addProfilesToStore(profiles.data)
                if (!size(profiles.data)) {
                    setHasNextPage(false)
                }

                setLoading(false)
            } catch (error) {
                enqueueSnackbar(`Browse people error: ${error.message}`, {variant: "error"})
            }
        }())
        // eslint-disable-next-line
    }, [
        needToLoad,
        addProfilesToStore,
        claims,
        enqueueSnackbar,
        getProfiles,
        limit,
        unwrap,
        setLoading,
        filterStore.firstName, filterStore.lastName,
        setHasNextPage,
    ])

    const selectProfilesWithFollowStatus = useMemo(() => {
        return createSelectProfilesWithFollowStatus(claims?.subId)
    }, [claims])

    return {
        profiles: values(useStore(selectProfilesWithFollowStatus)),
        infiniteRef,
    }
}

export const useFriends = () => {
    const {unwrap, getProfileFollowings} = useApi()
    const claims = useClaims()
    const {enqueueSnackbar} = useSnackbar()
    const setFollowings = useStore(useCallback(state => state.setFollowings, []))
    const [memorizedProfiles, memorizeProfiles] = useState<Record<string, models.Profile>>({})
    const [isFetched, setIsFetched] = useState(false)

    useEffect(() => {
        (async function () {
            if (!claims) return

            try {
                const followings = await unwrap(getProfileFollowings({id: claims.subId}))
                setFollowings(followings.data)
                setIsFetched(true)
            } catch (error) {
                enqueueSnackbar(`Fetch followings error: ${error.message}`, {variant: "error"})
            }
        }())
    }, [claims, enqueueSnackbar, getProfileFollowings, setFollowings, unwrap])

    const followings = useStore(selectFollowings)

    useEffect(() => {
        if (isFetched) {
            memorizeProfiles(followings)
        }
        // eslint-disable-next-line
    }, [isFetched])

    return useMemo(() => selectProfilesWithFollowStatus(
        {profiles: memorizedProfiles, followings}),
        [followings, memorizedProfiles],
    )
}

export const useFollow = () => {
    useRequiredAuth()

    const claims = useClaims()
    const {enqueueSnackbar} = useSnackbar()
    const {unwrap, putProfileFollowing, deleteProfileFollowing, getProfile} = useApi()
    const storeFollowing = useStore(useCallback(state => state.putFollowing, []))
    const unstoreFollowing = useStore(useCallback(state => state.deleteFollowing, []))

    return [
        useCallback(async (followingId) => {
            if (!claims) return
            try {
                await unwrap(putProfileFollowing({
                    followingId,
                    id: claims.subId,
                }))
                const profile = await unwrap(getProfile({
                    id: followingId,
                }))
                storeFollowing(profile)
            } catch (error) {
                enqueueSnackbar(`Follow error: ${error.message}`, {variant: "error"})
            }
        }, [claims, enqueueSnackbar, getProfile, putProfileFollowing, storeFollowing, unwrap]),
        useCallback(async (followingId) => {
            if (!claims) return

            try {
                await unwrap(deleteProfileFollowing({
                    followingId: followingId,
                    id: claims.subId,
                }))
                unstoreFollowing({
                    followingId: followingId,
                })
            } catch (error) {
                enqueueSnackbar(`Unfollow error: ${error.message}`, {variant: "error"})
            }
        }, [claims, deleteProfileFollowing, enqueueSnackbar, unstoreFollowing, unwrap]),
    ]
}
