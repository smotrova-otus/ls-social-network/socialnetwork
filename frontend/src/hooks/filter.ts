import {useFormik} from "formik";
import create from "zustand";

type Values = {
    firstName: string
    lastName: string
}

type Store = Values & {
    update: (values: Values) => void
}

const initialValues: Values = {
    firstName: '',
    lastName: ''
}

export const useFilterStore = create<Store>(set => ({
    ...initialValues,
    update: ({firstName, lastName}) => set(_ => ({
        firstName,
        lastName,
    }))
}))

export const useFilterForm = () => {
    const update = useFilterStore(state => state.update)

    return useFormik({
        initialValues,
        onSubmit: update,
    })
}
