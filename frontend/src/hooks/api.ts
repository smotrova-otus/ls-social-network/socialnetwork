import {createContext, useCallback, useContext, useMemo} from "react"
import axios, {AxiosRequestConfig, AxiosResponse} from "axios"
import * as models from "../models"
import {queryString} from "../lib/querystring";

export type Config = {
    baseURL: string
    getToken: () => string | undefined
}

export type ResponseError = {
    error?: string
}

export type PostAccountRequest = Partial<models.Account>
export type PostAccountResponse = models.Account & ResponseError

export type AuthenticateRequest = {
    login: string
    password: string
}
export type AuthenticateResponse = {
    token: string
} & ResponseError

export type GetAccountsRequest = {
    id: string
}
export type GetAccountsResponse = models.Account & ResponseError

export type PutProfileRequest = Partial<models.Profile> & {
    id: string
}
export type PutProfileResponse = models.Profile & ResponseError

export type GetProfileRequest = {
    id: string
}
export type GetProfileResponse = models.Profile & ResponseError

export type GetProfilesRequest = {
    firstName?: string
    lastName?: string
    limit?: number
    offset?: number
}
export type GetProfilesResponse = {
    data: models.Profile[]
} & ResponseError

export type GetProfileFollowersRequest = {
    id: string
}
export type GetProfileFollowersResponse = {
    data: models.Profile[]
} & ResponseError

export type PutProfileFollowingsRequest = {
    id: string
    followingId: string
}
export type PutProfileFollowingsResponse = models.Profile & ResponseError

export type DeleteProfileFollowingsRequest = {
    id: string
    followingId: string
}
export type DeleteProfileFollowingsResponse = ResponseError

export type GetProfileFollowingsRequest = {
    id: string
}
export type GetProfileFollowingsResponse = {
    data: models.Profile[]
} & ResponseError

export const localStorageTokenProvider = () => {
    const token = window.localStorage.getItem("token")
    return token ? JSON.parse(token) : null
}

export const ConfigContext = createContext<Config>({
    baseURL: "",
    getToken(): string | undefined {
        return undefined;
    }
})

const setAuthorization = (axiosConfig: AxiosRequestConfig, token?: string) => {
    return !token ? axiosConfig : {
        ...axiosConfig,
        headers: {
            ...axiosConfig.headers,
            "Authorization": `bearer ${token}`,
        }
    }
}

const setBaseURL = (axiosConfig: AxiosRequestConfig, baseURL: string) => {
    return {
        ...axiosConfig,
        baseURL
    }
}

async function unwrap<T extends ResponseError>(response: Promise<AxiosResponse<T>>): Promise<T> {
    try {
        const resp = await response
        return resp.data
    } catch (e) {
        if (e.response?.data.error) {
            throw new Error(e.response.data.error)
        }
        throw e
    }
}

export const useApi = () => {
    const apiConfig = useContext(ConfigContext)

    const axiosInstance = useCallback(() => {
        let axiosConfig: AxiosRequestConfig = {}
        axiosConfig = setAuthorization(axiosConfig, apiConfig.getToken())
        axiosConfig = setBaseURL(axiosConfig, apiConfig.baseURL)
        return axios.create(axiosConfig)
    }, [apiConfig])

    return useMemo(() => ({
        authenticate(request: AuthenticateRequest): Promise<AxiosResponse<AuthenticateResponse>> {
            return axiosInstance().post("/authenticate", request)
        },
        postAccount(request: PostAccountRequest): Promise<AxiosResponse<PostAccountResponse>> {
            return axiosInstance().post("/accounts", request)
        },
        getAccount(request: GetAccountsRequest): Promise<AxiosResponse<GetAccountsResponse>> {
            return axiosInstance().get(`/accounts/${request.id}`)
        },
        getProfile(request: GetProfileRequest): Promise<AxiosResponse<GetProfileResponse>> {
            return axiosInstance().get(`/profiles/${request.id}`)
        },
        putProfile(request: PutProfileRequest): Promise<AxiosResponse<PutProfileResponse>> {
            return axiosInstance().put(`/profiles/${request.id}`, request)
        },
        getProfiles(request?: GetProfilesRequest): Promise<AxiosResponse<GetProfilesResponse>> {
            return axiosInstance().get(`/profiles?${queryString(request)}`)
        },
        getProfileFollowers(request: GetProfileFollowersRequest): Promise<AxiosResponse<GetProfileFollowersResponse>> {
            return axiosInstance().get(`/profiles/${request.id}/followers`)
        },
        getProfileFollowings(request: GetProfileFollowingsRequest): Promise<AxiosResponse<GetProfileFollowingsResponse>> {
            return axiosInstance().get(`/profiles/${request.id}/followings`)
        },
        putProfileFollowing(request: PutProfileFollowingsRequest): Promise<AxiosResponse<PutProfileFollowingsResponse>> {
            return axiosInstance().put(`/profiles/${request.followingId}/followers/${request.id}`)
        },
        deleteProfileFollowing(request: DeleteProfileFollowingsRequest): Promise<AxiosResponse<DeleteProfileFollowingsResponse>> {
            return axiosInstance().delete(`/profiles/${request.followingId}/followers/${request.id}`)
        },
        unwrap
    }), [axiosInstance])
}
