import React, {FunctionComponent, useCallback} from "react";
import {Card, CardContent, Grid} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import {useFilterForm} from "../../hooks/filter";
import debounce from "lodash/debounce";

type PeopleFilterProps = {}

export const PeopleFilter: FunctionComponent<PeopleFilterProps> = (props) => {
    const form = useFilterForm()

    const debouncedSubmit = useCallback(debounce(() => {
        form.handleSubmit()
    }, 300), [form])

    const onChange = useCallback((e) => {
        form.handleChange(e)
        setTimeout(debouncedSubmit, 0)
    }, [form, debouncedSubmit])

    return (
        <form onSubmit={form.handleSubmit}>
            <Card>
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography gutterBottom variant="h5" component="h2">
                                Filter
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                autoComplete="firstName"
                                name="firstName"
                                variant="outlined"
                                fullWidth
                                id="firstName"
                                label="First Name"
                                onChange={onChange}
                                value={form.values.firstName}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                autoComplete="lastName"
                                name="lastName"
                                variant="outlined"
                                fullWidth
                                id="lastName"
                                label="Last Name"
                                onChange={onChange}
                                value={form.values.lastName}
                            />
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </form>
    )
}

