import React, {FunctionComponent, RefObject} from "react";
import map from "lodash/map";
import {Grid} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import * as models from "../../models";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import {formatDisplayDate} from "../../lib/date";
import {useFollow} from "../../hooks/logic";
import {NavLink} from "react-router-dom"

const useStyles = makeStyles(() => ({
    card: {
        marginBottom: 20,
        height: '100%',
        display: 'flex',
        flexDirection: "column",
    },
    cardContent: {
        flexGrow: 1
    }
}))

export type ProfileListProps = {
    profiles: models.ProfileWithFollowing[]
}

export const ProfileList: FunctionComponent<ProfileListProps> = (props) => {
    const {profiles} = props
    const classes = useStyles()
    const [follow, unfollow] = useFollow()

    return (
        <Grid container spacing={2}>
            {map(profiles, profile => (
                <Grid item xs={4} key={profile.id}>
                    <Card className={classes.card}>
                        <CardActionArea
                            component={NavLink}
                            className={classes.cardContent}
                            to={`/profile/${profile.id}`}
                        >
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {models.displayProfileName(profile)}
                                </Typography>
                                <Grid container direction="column">
                                    <Grid item>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            {formatDisplayDate(profile.birth_date)}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            {profile.city}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            {profile.isFollowing
                                ? (
                                    <Button
                                        size="small"
                                        color="primary"
                                        onClick={() => unfollow(profile.id)}
                                    >
                                        Unfollow
                                    </Button>
                                ) : (
                                    <Button
                                        size="small"
                                        color="primary"
                                        onClick={() => follow(profile.id)}
                                    >
                                        Follow
                                    </Button>
                                )
                            }
                        </CardActions>
                    </Card>
                </Grid>
            ))}
        </Grid>
    )
}
