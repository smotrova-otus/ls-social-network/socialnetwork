import React, {FunctionComponent} from 'react'
import CssBaseline from "@material-ui/core/CssBaseline"
import Container from "@material-ui/core/Container"
import makeStyles from "@material-ui/core/styles/makeStyles"

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
}))

type AuthLayoutProps = {
    children: React.ReactNode
}

export const AuthLayout: FunctionComponent<AuthLayoutProps> = (props: AuthLayoutProps) => {
    const { children } = props
    const classes = useStyles()

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                {children}
            </div>
        </Container>
    )
}
