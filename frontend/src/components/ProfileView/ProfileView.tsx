import React, {FunctionComponent} from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import * as models from "../../models"
import EditIcon from "@material-ui/icons/Edit"
import {Card, CardActions, CardContent} from "@material-ui/core"
import {formatDisplayDate} from "../../lib/date";

type ProfileViewProps = {
    profile: models.Profile
    isCanEditProfile: boolean
    handleEnableEditMode: () => void
}

export const ProfileView: FunctionComponent<ProfileViewProps> = (props: ProfileViewProps) => {
    const {profile, handleEnableEditMode, isCanEditProfile} = props

    return (
        <Card>
            <CardContent>
                <Grid container spacing={1}>
                    <Grid item xs={12}>
                        <Typography color="textSecondary">
                            {profile.gender}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography color="textSecondary">
                            {formatDisplayDate(profile.birth_date)}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography color="textSecondary">
                            {profile.city}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography color="textSecondary">
                            {profile.interests}
                        </Typography>
                    </Grid>
                </Grid>
            </CardContent>
            <CardActions>
                {isCanEditProfile && (
                    <Button
                        startIcon={<EditIcon/>}
                        size="small"
                        color="primary"
                        onClick={handleEnableEditMode}
                    >Edit</Button>
                )}
            </CardActions>
        </Card>
    )
}
