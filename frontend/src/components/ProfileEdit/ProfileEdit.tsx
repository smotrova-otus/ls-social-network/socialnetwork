import React, {FunctionComponent} from "react"
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import {
    Button, Card,
    CardActions,
    CardContent,
    FormControl,
    InputLabel,
    MenuItem,
    Select
} from "@material-ui/core"
import {KeyboardDatePicker} from "@material-ui/pickers"
import * as models from "../../models"
import {useProfileForm} from "../../hooks/logic"
import {displayDateFormat} from "../../lib/date";


type UserEditProps = {
    profile: models.Profile
    handleDisableEditMode: () => void
}

export const ProfileEdit: FunctionComponent<UserEditProps> = (props: UserEditProps) => {
    const {profile, handleDisableEditMode} = props
    const form = useProfileForm({
        profile,
        onSuccessCallback: handleDisableEditMode
    })

    return (
        <form onSubmit={form.handleSubmit}>
            <Card>
                <CardContent>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <TextField
                                autoComplete="first_name"
                                name="first_name"
                                variant="outlined"
                                required
                                fullWidth
                                id="first_name"
                                label="First Name"
                                autoFocus
                                onChange={form.handleChange}
                                value={form.values.first_name}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                autoComplete="last_name"
                                name="last_name"
                                variant="outlined"
                                required
                                fullWidth
                                id="last_name"
                                label="Last Name"
                                onChange={form.handleChange}
                                value={form.values.last_name}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl fullWidth variant="outlined">
                                <InputLabel id="user-edit-sex-label">Sex</InputLabel>
                                <Select
                                    label="Sex"
                                    name="gender"
                                    labelId="user-edit-sex-label"
                                    value={form.values.gender}
                                    onChange={form.handleChange}
                                >
                                    <MenuItem value="male">Male</MenuItem>
                                    <MenuItem value="female">Female</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format={displayDateFormat}
                                margin="normal"
                                name="birth_date"
                                label="Birthdate"
                                value={form.values.birth_date}
                                onChange={val => {
                                    form.setFieldValue("birth_date", val)
                                }}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                autoComplete="city"
                                name="city"
                                variant="outlined"
                                fullWidth
                                id="city"
                                label="City"
                                value={form.values.city}
                                onChange={form.handleChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                autoComplete="interests"
                                name="interests"
                                variant="outlined"
                                fullWidth
                                id="interests"
                                label="Interests"
                                value={form.values.interests}
                                onChange={form.handleChange}
                            />
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions>
                    <Grid container>
                        <Grid item>
                            <Button
                                size="small"
                                color="primary"
                                type="submit"
                            >
                                Save
                            </Button>
                            <Button
                                size="small"
                                color="primary"
                                onClick={handleDisableEditMode}
                            >
                                Cancel
                            </Button>
                        </Grid>
                    </Grid>
                </CardActions>
            </Card>
        </form>
    )
}
