import React, {FunctionComponent} from 'react'
import Container from "@material-ui/core/Container"
import makeStyles from "@material-ui/core/styles/makeStyles"
import MenuList from "@material-ui/core/MenuList"
import MenuItem from "@material-ui/core/MenuItem"
import Typography from "@material-ui/core/Typography"
import Paper from "@material-ui/core/Paper"
import PeopleIcon from '@material-ui/icons/People'
import SearchIcon from '@material-ui/icons/Search'
import ListItemIcon from "@material-ui/core/ListItemIcon"
import HomeIcon from '@material-ui/icons/Home'
import {Grid} from "@material-ui/core"
import {NavLink} from "react-router-dom"
import AppBar from "@material-ui/core/AppBar"
import Button from "@material-ui/core/Button"
import Toolbar from '@material-ui/core/Toolbar'
import {useLogout} from "../../hooks/auth"

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        flexGrow: 1,
    },
}))

type ProfileLayoutProps = {
    children: React.ReactNode
}

export const Layout: FunctionComponent<ProfileLayoutProps> = (props: ProfileLayoutProps) => {
    const {children} = props
    const classes = useStyles()
    const logout = useLogout()

    return (
        <Container component="main" maxWidth="md">
            <AppBar position="sticky" color="default">
                <Toolbar>
                    <Typography className={classes.title} variant="h6">
                        SocialNetwork
                    </Typography>
                    <Button color="inherit" onClick={logout}>Logout</Button>
                </Toolbar>
            </AppBar>
            <div className={classes.paper}>
                <Grid container spacing={2}>
                    <Grid item xs={3} sm={3}>
                        <Paper>
                            <MenuList>
                                <MenuItem divider component={NavLink} to="/" activeClassName="Mui-selected" exact>
                                    <ListItemIcon>
                                        <HomeIcon fontSize="small"/>
                                    </ListItemIcon>
                                    <Typography variant="inherit">Home</Typography>
                                </MenuItem>
                                <MenuItem component={NavLink} to="/userFriends" activeClassName="Mui-selected" exact>
                                    <ListItemIcon>
                                        <PeopleIcon fontSize="small"/>
                                    </ListItemIcon>
                                    <Typography variant="inherit">Friends</Typography>
                                </MenuItem>
                                <MenuItem component={NavLink} to="/browse" activeClassName="Mui-selected" exact>
                                    <ListItemIcon>
                                        <SearchIcon fontSize="small"/>
                                    </ListItemIcon>
                                    <Typography variant="inherit">Browse</Typography>
                                </MenuItem>
                            </MenuList>
                        </Paper>
                    </Grid>
                    <Grid item xs={9} sm={9}>
                        {children}
                    </Grid>
                </Grid>
            </div>
        </Container>
    )
}
