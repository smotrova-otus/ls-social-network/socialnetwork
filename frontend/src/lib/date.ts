import {format} from "date-fns"

export const serverDateFormat = 'yyyy-MM-dd'
export const displayDateFormat = 'yyyy-MM-dd'

export const formatServerDate = (date?: string) => {
    if (!date) {
        return undefined
    }
    return format(new Date(date), serverDateFormat)
}

export const formatDisplayDate = (date?: string) => {
    if (!date) {
        return undefined
    }
    return format(new Date(date), displayDateFormat)
}
