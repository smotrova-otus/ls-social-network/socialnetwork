import React, {FunctionComponent} from 'react'
import makeStyles from "@material-ui/core/styles/makeStyles"
import {Grid} from "@material-ui/core"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import {AuthLayout} from "../../components/AuthLayout/AuthLayout"
import Avatar from "@material-ui/core/Avatar"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import Typography from "@material-ui/core/Typography"
import {useLoginForm} from "../../hooks/logic"
import {useNavigation} from "../../hooks/navigation"

const useStyles = makeStyles((theme) => ({
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}))

type AuthSignInProps = {}

export const AuthSignIn: FunctionComponent<AuthSignInProps> = () => {
    const classes = useStyles()
    const {navigateSignUp} = useNavigation()
    const form = useLoginForm()

    return (
        <AuthLayout>
            <Avatar className={classes.avatar}>
                <LockOutlinedIcon/>
            </Avatar>
            <Typography component="h1" variant="h5">
                Sign in
            </Typography>
            <form className={classes.form} onSubmit={form.handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            autoComplete="login"
                            name="login"
                            variant="outlined"
                            required
                            fullWidth
                            id="login"
                            label="Email Address"
                            type="email"
                            autoFocus
                            onChange={form.handleChange}
                            value={form.values.login}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            autoComplete="password"
                            name="password"
                            variant="outlined"
                            required
                            fullWidth
                            type="password"
                            id="password"
                            label="Password"
                            onChange={form.handleChange}
                            value={form.values.password}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign In
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            fullWidth
                            color="default"
                            onClick={navigateSignUp}
                            className={classes.submit}
                        >
                            Sign Up
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </AuthLayout>
    )
}
