import React, {FunctionComponent} from 'react'
import makeStyles from "@material-ui/core/styles/makeStyles"
import Avatar from "@material-ui/core/Avatar"
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from "@material-ui/core/Typography"
import {FormControl, Grid, InputLabel, MenuItem, Select} from "@material-ui/core"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import {AuthLayout} from "../../components/AuthLayout/AuthLayout"
import {useRegisterForm} from "../../hooks/logic"
import {useNavigation} from "../../hooks/navigation"

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}))

type AuthSignUpProps = {}

export const AuthSignUp: FunctionComponent<AuthSignUpProps> = () => {
    const classes = useStyles()
    const { navigateSignIn } = useNavigation()
    const form = useRegisterForm()

    return (
        <AuthLayout>
            <Avatar className={classes.avatar}>
                <LockOutlinedIcon/>
            </Avatar>
            <Typography component="h1" variant="h5">
                Sign up
            </Typography>
            <form className={classes.form} onSubmit={form.handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            autoComplete="first_name"
                            name="first_name"
                            variant="outlined"
                            required
                            fullWidth
                            id="first_name"
                            label="First Name"
                            autoFocus
                            onChange={form.handleChange}
                            value={form.values.first_name}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            autoComplete="last_name"
                            name="last_name"
                            variant="outlined"
                            required
                            fullWidth
                            id="last_name"
                            label="Last Name"
                            onChange={form.handleChange}
                            value={form.values.last_name}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            autoComplete="email"
                            name="login"
                            variant="outlined"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            type="email"
                            onChange={form.handleChange}
                            value={form.values.login}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            autoComplete="password"
                            name="password"
                            variant="outlined"
                            required
                            fullWidth
                            type="password"
                            id="password"
                            label="Password"
                            onChange={form.handleChange}
                            value={form.values.password}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <FormControl fullWidth variant="outlined">
                            <InputLabel id="user-edit-sex-label">Sex</InputLabel>
                            <Select
                                label="Sex"
                                name="gender"
                                labelId="user-edit-sex-label"
                                value={form.values.gender}
                                onChange={form.handleChange}
                            >
                                <MenuItem value="male">Male</MenuItem>
                                <MenuItem value="female">Female</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign Up
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            fullWidth
                            color="default"
                            className={classes.submit}
                            onClick={navigateSignIn}
                        >
                            Sign In
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </AuthLayout>
    )
}
