import React, {FunctionComponent, RefObject} from "react";
import Typography from "@material-ui/core/Typography";
import {Grid} from "@material-ui/core";
import {ProfileList} from "../../components/ProfileList/ProfileList";
import {Layout} from "../../components/Layout/Layout";
import {useBrowsePeople} from "../../hooks/logic";
import {PeopleFilter} from "../../components/PeopleFilter/PeopleFilter";

type BrowsePeopleProps = {
}

export const BrowsePeople: FunctionComponent<BrowsePeopleProps> = (props) => {
    const {profiles, infiniteRef} = useBrowsePeople(15)

    return (
        <Layout>
            <Typography gutterBottom variant="h3" component="h1">
                Browse people
            </Typography>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <PeopleFilter />
                </Grid>

                <Grid item xs={12}>
                    <div ref={infiniteRef as RefObject<HTMLDivElement>}>
                        <ProfileList profiles={profiles}/>
                    </div>
                </Grid>
            </Grid>
        </Layout>
    )
}
