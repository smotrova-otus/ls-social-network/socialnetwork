import React, {FunctionComponent} from "react"
import {Layout} from "../../components/Layout/Layout"
import Typography from "@material-ui/core/Typography"
import {Grid} from "@material-ui/core"
import {ProfileList} from "../../components/ProfileList/ProfileList";
import {useFriends} from "../../hooks/logic";

type FriendsProps = {}

export const Friends: FunctionComponent<FriendsProps> = (props: FriendsProps) => {
    const profiles = useFriends()

    return (
        <Layout>
            <Typography gutterBottom variant="h3" component="h1">
                Friends
            </Typography>
            <Grid container spacing={2}>
                <ProfileList profiles={profiles}/>
            </Grid>
        </Layout>
    )
}
