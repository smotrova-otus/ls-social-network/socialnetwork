import React, {FunctionComponent, useState} from "react"
import {Layout} from "../../components/Layout/Layout"
import {ProfileEdit} from "../../components/ProfileEdit/ProfileEdit"
import * as models from "../../models"
import {ProfileView} from "../../components/ProfileView/ProfileView"
import Typography from "@material-ui/core/Typography"
import {useProfile} from "../../hooks/logic"
import {useParams} from "react-router-dom"

type ProfileProps = {}

export const Profile: FunctionComponent<ProfileProps> = (props: ProfileProps) => {
    const {id} = useParams()
    const {profile, isCanEditProfile} = useProfile(id)
    const [editMode, setEditMode] = useState(false)

    if (!profile) {
        return null
    }

    const handleEnableEditMode = () => setEditMode(true)
    const handleDisableEditMode = () => setEditMode(false)

    return (
        <Layout>
            <Typography gutterBottom variant="h3" component="h1">
                {models.displayProfileName(profile)}
            </Typography>
            {editMode
                ? <ProfileEdit
                    profile={profile}
                    handleDisableEditMode={handleDisableEditMode}
                />
                : <ProfileView isCanEditProfile={isCanEditProfile}
                               profile={profile}
                               handleEnableEditMode={handleEnableEditMode}
                />}
        </Layout>
    )
}
