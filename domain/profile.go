package domain

import (
	"fmt"
	"net/http"
	"strings"
	"unicode/utf8"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/types"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type GenderType string

const (
	GenderTypeMale   GenderType = "male"
	GenderTypeFemale GenderType = "female"
)

type Profile struct {
	ID        uuid.UUID   `db:"id" json:"id"`
	FirstName string      `db:"first_name" json:"first_name"`
	LastName  string      `db:"last_name" json:"last_name"`
	BirthDate *types.Date `db:"birth_date" json:"birth_date"`
	Gender    GenderType  `db:"gender" json:"gender"`
	Interests string      `db:"interests" json:"interests"`
	City      string      `db:"city" json:"city"`
}

func (dto *Profile) Modify() {
	dto.FirstName = strings.Trim(dto.FirstName, " ")
	dto.LastName = strings.Trim(dto.LastName, " ")
	dto.Interests = strings.Trim(dto.Interests, " ")
	dto.City = strings.Trim(dto.City, " ")
}

var (
	ErrInvalidFirstNameLength = &errors.Error{
		Err:    "invalid first_name length",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
	ErrInvalidLastNameLength = &errors.Error{
		Err:    "invalid last_name length",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
	ErrInvalidCityLength = &errors.Error{
		Err:    "invalid city length",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
	ErrInvalidInterestsLength = &errors.Error{
		Err:    "invalid interests length",
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
	ErrIncorrectGender = &errors.Error{
		Err:    fmt.Sprintf("incorrect gender type: may only %s, %s", GenderTypeFemale, GenderTypeMale),
		Code:   "BAD_REQUEST",
		Status: http.StatusBadRequest,
	}
)

// TODO add test for validate function
func (dto *Profile) Validate() error {
	l := utf8.RuneCountInString(dto.FirstName)
	if l < 1 || l > 255 {
		return ErrInvalidFirstNameLength
	}
	l = utf8.RuneCountInString(dto.LastName)
	if l < 1 || l > 255 {
		return ErrInvalidLastNameLength
	}
	l = utf8.RuneCountInString(dto.City)
	if l > 255 {
		return ErrInvalidCityLength
	}
	l = utf8.RuneCountInString(dto.City)
	if l > 1024 {
		return ErrInvalidInterestsLength
	}
	if dto.Gender != GenderTypeMale && dto.Gender != GenderTypeFemale {
		return ErrIncorrectGender
	}
	return nil
}
