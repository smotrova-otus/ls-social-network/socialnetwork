package domain

import "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"

type Account struct {
	ID           uuid.UUID `db:"id" json:"id"`
	Login        string    `db:"login" json:"login"`
	PasswordHash []byte    `db:"password_hash" json:"-"`
	PasswordSalt []byte    `db:"password_salt" json:"-"`
}
