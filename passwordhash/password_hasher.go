package passwordhash

import (
	crand "crypto/rand"
	"crypto/sha256"
	"fmt"
	"golang.org/x/crypto/hkdf"
	"golang.org/x/crypto/scrypt"
	"io"
)

// PasswordHasher generates password hash.
type PasswordHasher interface {
	CreateHash(password, salt []byte) ([]byte, error)
	MakeSalt() (salt []byte, err error)
}

type PasswordHasherImpl struct{}

// NewPasswordHasherImpl is a constructor.
func NewPasswordHasherImpl() *PasswordHasherImpl {
	return &PasswordHasherImpl{}
}

// CreateHash generates password hash.
func (p PasswordHasherImpl) CreateHash(password, salt []byte) ([]byte, error) {
	count := 65536

	hash, err := scrypt.Key(password, salt, count, 8, 1, 32)
	if err != nil {
		return nil, fmt.Errorf("scrypt.Key() error: %w", err)
	}
	reader := hkdf.New(sha256.New, hash, nil, nil)
	if _, err := io.ReadFull(reader, hash); err != nil {
		return nil, fmt.Errorf("hash readFull error: %w", err)
	}

	return hash, nil
}

// MakeSalt generates salt.
func (p PasswordHasherImpl) MakeSalt() (salt []byte, err error) {
	salt = make([]byte, 32)
	_, err = crand.Read(salt)
	if err != nil {
		return nil, err
	}
	return salt, nil
}
