package profile

import (
	"context"
	"errors"
	"github.com/go-kit/kit/auth/jwt"
	"github.com/stretchr/testify/require"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth/token"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"testing"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	pkgerrors "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
)

func Test_service_Read(t *testing.T) {
	ctx, _ := log.Default()
	profileRepositoryMock := newProfileRepositoryMock()
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name       string
		args       args
		wantResult *domain.Profile
		wantErr    error
		setup      func()
	}{
		{
			name: "success",
			setup: func() {
				profileRepositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					&domain.Profile{
						ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						FirstName: "test first name",
						LastName:  "test last name",
						City:      "city",
						Interests: "interests",
						Gender:    "male",
						BirthDate: nil,
					},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantResult: &domain.Profile{
				ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
				FirstName: "test first name",
				LastName:  "test last name",
				BirthDate: nil,
				Gender:    "male",
				Interests: "interests",
				City:      "city",
			},
			wantErr: nil,
		},
		{
			name: "not_found",
			setup: func() {
				profileRepositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantResult: nil,
			wantErr:    pkgerrors.ErrNotFound,
		},
		{
			name: "find_error",
			setup: func() {
				profileRepositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
					errors.New("some error"),
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantResult: nil,
			wantErr:    errors.New("some error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				profileRepositoryMock.AssertExpectations(t)
				profileRepositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				profileRepositoryMock,
				nil,
			)
			gotResult, err := s.Read(tt.args.ctx, tt.args.id)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_List(t *testing.T) {
	ctx, _ := log.Default()
	profileRepositoryMock := newProfileRepositoryMock()
	type args struct {
		ctx    context.Context
		filter filter
		offset int64
		limit  int
	}
	tests := []struct {
		name       string
		args       args
		wantResult []domain.Profile
		wantErr    error
		setup      func()
	}{
		{
			name: "success",
			setup: func() {
				profileRepositoryMock.setFindAllWithFilter(
					0,
					10,
					filter{
						LastName: "test last name",
					},
					[]domain.Profile{
						{
							ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
							FirstName: "test first name",
							LastName:  "test last name",
							BirthDate: nil,
							Gender:    "male",
							Interests: "interests",
							City:      "city",
						},
					},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				filter: filter{
					LastName: "test last name",
				},
				offset: 0,
				limit:  10,
			},
			wantResult: []domain.Profile{
				{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					FirstName: "test first name",
					LastName:  "test last name",
					BirthDate: nil,
					Gender:    "male",
					Interests: "interests",
					City:      "city",
				},
			},
			wantErr: nil,
		},
		{
			name: "empty",
			setup: func() {
				profileRepositoryMock.setFindAllWithFilter(
					0,
					10,
					filter{
						LastName: "test last name",
					},
					[]domain.Profile{},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				filter: filter{
					LastName: "test last name",
				},
				offset: 0,
				limit:  10,
			},
			wantResult: []domain.Profile{},
			wantErr:    nil,
		},
		{
			name: "find_error",
			setup: func() {
				profileRepositoryMock.setFindAllWithFilter(
					0,
					10,
					filter{
						LastName: "test last name",
					},
					[]domain.Profile{},
					errors.New("some error"),
				)
			},
			args: args{
				ctx: ctx,
				filter: filter{
					LastName: "test last name",
				},
				offset: 0,
				limit:  10,
			},
			wantResult: nil,
			wantErr:    errors.New("some error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				profileRepositoryMock.AssertExpectations(t)
				profileRepositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				profileRepositoryMock,
				nil,
			)
			gotResult, err := s.List(tt.args.ctx, tt.args.limit, tt.args.offset, tt.args.filter)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_Upsert(t *testing.T) {
	ctx, _ := log.Default()
	ctx = context.WithValue(ctx, jwt.JWTClaimsContextKey, &token.Claims{
		SubjectID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
	})
	repositoryMock := newProfileRepositoryMock()
	type args struct {
		ctx context.Context
		id  uuid.UUID
		dto domain.Profile
	}
	tests := []struct {
		name       string
		args       args
		wantResult *domain.Profile
		wantErr    error
		setup      func()
	}{
		{
			name: "validation_err",
			setup: func() {
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				dto: domain.Profile{
					FirstName: "",
					LastName:  "test last name",
					City:      "city",
					Interests: "interests",
					Gender:    "male",
					BirthDate: nil,
				},
			},
			wantResult: nil,
			wantErr:    domain.ErrInvalidFirstNameLength,
		},
		{
			name: "success_insert",
			setup: func() {
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					nil,
					nil,
				)
				repositoryMock.setInsert(
					&domain.Profile{
						ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
						FirstName: "test first name",
						LastName:  "test last name",
						City:      "city",
						Interests: "interests",
						Gender:    "male",
						BirthDate: nil,
					},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				dto: domain.Profile{
					FirstName: "test first name",
					LastName:  "test last name",
					City:      "city",
					Interests: "interests",
					Gender:    "male",
					BirthDate: nil,
				},
			},
			wantResult: &domain.Profile{
				ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				FirstName: "test first name",
				LastName:  "test last name",
				City:      "city",
				Interests: "interests",
				Gender:    "male",
				BirthDate: nil,
			},
			wantErr: nil,
		},
		{
			name: "success_update",
			setup: func() {
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					&domain.Profile{
						ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
						FirstName: "first name",
						LastName:  "last name",
						City:      "c",
						Interests: "i",
						Gender:    "female",
						BirthDate: nil,
					},
					nil,
				)
				repositoryMock.setUpdate(
					&domain.Profile{
						ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
						FirstName: "test first name",
						LastName:  "test last name",
						City:      "city",
						Interests: "interests",
						Gender:    "male",
						BirthDate: nil,
					},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				dto: domain.Profile{
					FirstName: "test first name",
					LastName:  "test last name",
					City:      "city",
					Interests: "interests",
					Gender:    "male",
					BirthDate: nil,
				},
			},
			wantResult: &domain.Profile{
				ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				FirstName: "test first name",
				LastName:  "test last name",
				City:      "city",
				Interests: "interests",
				Gender:    "male",
				BirthDate: nil,
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				repositoryMock.AssertExpectations(t)
				repositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				repositoryMock,
				nil,
			)
			gotResult, err := s.Upsert(tt.args.ctx, tt.args.id, tt.args.dto)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_AddFollower(t *testing.T) {
	ctx, _ := log.Default()
	ctx = context.WithValue(ctx, jwt.JWTClaimsContextKey, &token.Claims{
		SubjectID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
	})
	repositoryMock := newProfileRepositoryMock()
	type args struct {
		ctx        context.Context
		id         uuid.UUID
		followerID uuid.UUID
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
		setup   func()
	}{
		{
			name: "success",
			setup: func() {
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					&domain.Profile{
						ID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					},
					nil,
				)
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					&domain.Profile{
						ID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					},
					nil,
				)
				repositoryMock.setAddFollower(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
				)
			},
			args: args{
				ctx:        ctx,
				id:         uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: nil,
		},
		{
			name: "not_found_profile_id",
			setup: func() {
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					nil,
					nil,
				)
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					&domain.Profile{
						ID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					},
					nil,
				)
				repositoryMock.setAddFollower(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
				)
			},
			args: args{
				ctx:        ctx,
				id:         uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: pkgerrors.ErrNotFound,
		},
		{
			name: "not_found_follower",
			setup: func() {
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					&domain.Profile{
						ID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					},
					nil,
				)
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
					nil,
				)
				repositoryMock.setAddFollower(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
				)
			},
			args: args{
				ctx:        ctx,
				id:         uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: pkgerrors.ErrNotFound,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				repositoryMock.AssertExpectations(t)
				repositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				repositoryMock,
				nil,
			)
			err := s.AddFollower(tt.args.ctx, tt.args.id, tt.args.followerID)

			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_RemoveFollower(t *testing.T) {
	ctx, _ := log.Default()
	ctx = context.WithValue(ctx, jwt.JWTClaimsContextKey, &token.Claims{
		SubjectID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
	})
	repositoryMock := newProfileRepositoryMock()
	type args struct {
		ctx        context.Context
		id         uuid.UUID
		followerID uuid.UUID
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
		setup   func()
	}{
		{
			name: "success",
			setup: func() {
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					&domain.Profile{
						ID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					},
					nil,
				)
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					&domain.Profile{
						ID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					},
					nil,
				)
				repositoryMock.setDeleteFollower(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
				)
			},
			args: args{
				ctx:        ctx,
				id:         uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: nil,
		},
		{
			name: "not_found_profile_id",
			setup: func() {
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					nil,
					nil,
				)
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					&domain.Profile{
						ID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					},
					nil,
				)
				repositoryMock.setDeleteFollower(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
				)
			},
			args: args{
				ctx:        ctx,
				id:         uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: pkgerrors.ErrNotFound,
		},
		{
			name: "not_found_follower",
			setup: func() {
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					&domain.Profile{
						ID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					},
					nil,
				)
				repositoryMock.setFindByID(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
					nil,
				)
				repositoryMock.setDeleteFollower(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					nil,
				)
			},
			args: args{
				ctx:        ctx,
				id:         uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: pkgerrors.ErrNotFound,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				repositoryMock.AssertExpectations(t)
				repositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				repositoryMock,
				nil,
			)
			err := s.RemoveFollower(tt.args.ctx, tt.args.id, tt.args.followerID)

			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_ListFollowers(t *testing.T) {
	ctx, _ := log.Default()
	profileRepositoryMock := newProfileRepositoryMock()
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name       string
		args       args
		wantResult []domain.Profile
		wantErr    error
		setup      func()
	}{
		{
			name: "success",
			setup: func() {
				profileRepositoryMock.setFindFollowers(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					[]domain.Profile{
						{
							ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
							FirstName: "test first name",
							LastName:  "test last name",
							BirthDate: nil,
							Gender:    "male",
							Interests: "interests",
							City:      "city",
						},
					},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
			},
			wantResult: []domain.Profile{
				{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					FirstName: "test first name",
					LastName:  "test last name",
					BirthDate: nil,
					Gender:    "male",
					Interests: "interests",
					City:      "city",
				},
			},
			wantErr: nil,
		},
		{
			name: "empty",
			setup: func() {
				profileRepositoryMock.setFindFollowers(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					[]domain.Profile{},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
			},
			wantResult: []domain.Profile{},
			wantErr:    nil,
		},
		{
			name: "find_error",
			setup: func() {
				profileRepositoryMock.setFindFollowers(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					[]domain.Profile{},
					errors.New("some error"),
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
			},
			wantResult: nil,
			wantErr:    errors.New("some error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				profileRepositoryMock.AssertExpectations(t)
				profileRepositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				profileRepositoryMock,
				nil,
			)
			gotResult, err := s.ListFollowers(tt.args.ctx, tt.args.id)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_service_ListFollowings(t *testing.T) {
	ctx, _ := log.Default()
	profileRepositoryMock := newProfileRepositoryMock()
	type args struct {
		ctx context.Context
		id  uuid.UUID
	}
	tests := []struct {
		name       string
		args       args
		wantResult []domain.Profile
		wantErr    error
		setup      func()
	}{
		{
			name: "success",
			setup: func() {
				profileRepositoryMock.setFindFollowings(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					[]domain.Profile{
						{
							ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
							FirstName: "test first name",
							LastName:  "test last name",
							BirthDate: nil,
							Gender:    "male",
							Interests: "interests",
							City:      "city",
						},
					},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
			},
			wantResult: []domain.Profile{
				{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					FirstName: "test first name",
					LastName:  "test last name",
					BirthDate: nil,
					Gender:    "male",
					Interests: "interests",
					City:      "city",
				},
			},
			wantErr: nil,
		},
		{
			name: "empty",
			setup: func() {
				profileRepositoryMock.setFindFollowings(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					[]domain.Profile{},
					nil,
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
			},
			wantResult: []domain.Profile{},
			wantErr:    nil,
		},
		{
			name: "find_error",
			setup: func() {
				profileRepositoryMock.setFindFollowings(
					uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					[]domain.Profile{},
					errors.New("some error"),
				)
			},
			args: args{
				ctx: ctx,
				id:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
			},
			wantResult: nil,
			wantErr:    errors.New("some error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			defer func() {
				profileRepositoryMock.AssertExpectations(t)
				profileRepositoryMock.ExpectedCalls = nil
			}()
			s := NewService(
				profileRepositoryMock,
				nil,
			)
			gotResult, err := s.ListFollowings(tt.args.ctx, tt.args.id)

			require.Equal(t, tt.wantResult, gotResult)
			if newErr := errors.Unwrap(err); newErr != nil {
				err = newErr
			}
			require.Equal(t, tt.wantErr, err)
		})
	}
}
