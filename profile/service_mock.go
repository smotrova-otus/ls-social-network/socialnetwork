package profile

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type serviceMock struct {
	mock.Mock
}

func (m *serviceMock) Upsert(ctx context.Context, id uuid.UUID, profile domain.Profile) (result *domain.Profile, err error) {
	results := m.Called(ctx, id, profile)
	return results.Get(0).(*domain.Profile), results.Error(1)
}

func (m *serviceMock) AddFollower(ctx context.Context, id, followingProfileID uuid.UUID) error {
	results := m.Called(ctx, id, followingProfileID)
	return results.Error(0)
}

func (m *serviceMock) RemoveFollower(ctx context.Context, id, followingProfileID uuid.UUID) error {
	results := m.Called(ctx, id, followingProfileID)
	return results.Error(0)
}

func (m *serviceMock) ListFollowings(ctx context.Context, id uuid.UUID) ([]domain.Profile, error) {
	results := m.Called(ctx, id)
	return results.Get(0).([]domain.Profile), results.Error(1)
}

func (m *serviceMock) ListFollowers(ctx context.Context, id uuid.UUID) ([]domain.Profile, error) {
	results := m.Called(ctx, id)
	return results.Get(0).([]domain.Profile), results.Error(1)
}

func newServiceMock() *serviceMock {
	return &serviceMock{}
}

// Read implementation.
func (m *serviceMock) Read(ctx context.Context, id uuid.UUID) (result *domain.Profile, err error) {
	results := m.Called(ctx, id)
	return results.Get(0).(*domain.Profile), results.Error(1)
}

// List implementation.
func (m *serviceMock) List(ctx context.Context, limit int, offset int64, filter filter) ([]domain.Profile, error) {
	results := m.Called(ctx, limit, offset, filter)
	return results.Get(0).([]domain.Profile), results.Error(1)
}

// setRead sets mock for method Read.
func (m *serviceMock) setRead(id uuid.UUID, acc *domain.Profile, err error) {
	m.On(
		"Read",
		mock.Anything,
		id,
	).Return(acc, err).Once()
}

// setUpsert sets mock for method Upsert.
func (m *serviceMock) setUpsert(id uuid.UUID, fields domain.Profile, profile *domain.Profile, err error) {
	m.On(
		"Upsert",
		mock.Anything,
		id,
		fields,
	).Return(profile, err).Once()
}

// setList sets mock for method List.
func (m *serviceMock) setList(limit int, offset int64, f filter, profiles []domain.Profile, err error) {
	m.On(
		"List",
		mock.Anything,
		limit,
		offset,
		f,
	).Return(profiles, err).Once()
}

// setAddFollower sets mock for method AddFollowing.
func (m *serviceMock) setAddFollower(id, following uuid.UUID, err error) {
	m.On(
		"AddFollower",
		mock.Anything,
		id,
		following,
	).Return(err).Once()
}

// setRemoveFollower sets mock for method RemoveFollowing.
func (m *serviceMock) setRemoveFollower(id, following uuid.UUID, err error) {
	m.On(
		"RemoveFollower",
		mock.Anything,
		id,
		following,
	).Return(err).Once()
}

// setListFollowings sets mock for method ListFollowings.
func (m *serviceMock) setListFollowings(id uuid.UUID, res []domain.Profile, err error) {
	m.On(
		"ListFollowings",
		mock.Anything,
		id,
	).Return(res, err).Once()
}

// setListFollowers sets mock for method ListFollowers.
func (m *serviceMock) setListFollowers(id uuid.UUID, res []domain.Profile, err error) {
	m.On(
		"ListFollowers",
		mock.Anything,
		id,
	).Return(res, err).Once()
}
