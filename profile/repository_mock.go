package profile

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type profileRepositoryMock struct {
	mock.Mock
}

// newProfileRepositoryMock is a constructor.
func newProfileRepositoryMock() *profileRepositoryMock {
	return &profileRepositoryMock{}
}

// FindByID implementation.
func (m *profileRepositoryMock) FindByID(id uuid.UUID) (*domain.Profile, error) {
	results := m.Called(id)
	return results.Get(0).(*domain.Profile), results.Error(1)
}

// Insert implementation.
func (m *profileRepositoryMock) Insert(profile *domain.Profile) error {
	results := m.Called(profile)
	return results.Error(0)
}

// Update implementation.
func (m *profileRepositoryMock) Update(profile *domain.Profile) error {
	results := m.Called(profile)
	return results.Error(0)
}

// FindAllWithFilter implementation.
func (m *profileRepositoryMock) FindAllWithFilter(offset int64, limit int, f filter) ([]domain.Profile, error) {
	results := m.Called(offset, limit, f)
	return results.Get(0).([]domain.Profile), results.Error(1)
}

func (m *profileRepositoryMock) AddFollower(profileID, followerID uuid.UUID) error {
	results := m.Called(profileID, followerID)
	return results.Error(0)
}

func (m *profileRepositoryMock) DeleteFollower(profileID, followerID uuid.UUID) error {
	results := m.Called(profileID, followerID)
	return results.Error(0)
}

func (m *profileRepositoryMock) FindFollowers(id uuid.UUID) ([]domain.Profile, error) {
	results := m.Called(id)
	return results.Get(0).([]domain.Profile), results.Error(1)
}

func (m *profileRepositoryMock) FindFollowings(id uuid.UUID) ([]domain.Profile, error) {
	results := m.Called(id)
	return results.Get(0).([]domain.Profile), results.Error(1)
}

// setAddFollower set mock for method AddFollower.
func (m *profileRepositoryMock) setAddFollower(id, followerID uuid.UUID, err error) {
	m.On("AddFollower", id, followerID).
		Return(err)
}

// setDeleteFollower set mock for method DeleteFollower.
func (m *profileRepositoryMock) setDeleteFollower(id, followerID uuid.UUID, err error) {
	m.On("DeleteFollower", id, followerID).
		Return(err)
}

// setFindByID set mock for method FindByID.
func (m *profileRepositoryMock) setFindByID(id uuid.UUID, acc *domain.Profile, err error) {
	m.On("FindByID", id).
		Return(acc, err)
}

// setInsert set mock for method Insert.
func (m *profileRepositoryMock) setInsert(acc *domain.Profile, err error) {
	m.On("Insert", acc).
		Return(err)
}

// setUpdate set mock for method Update.
func (m *profileRepositoryMock) setUpdate(acc *domain.Profile, err error) {
	m.On("Update", acc).
		Return(err)
}

// setFindAllWithFilter set mock for method FindAllWithFilter.
func (m *profileRepositoryMock) setFindAllWithFilter(
	offset int64,
	limit int,
	f filter,
	profiles []domain.Profile,
	err error) {
	m.On("FindAllWithFilter", offset, limit, f).
		Return(profiles, err)
}

// setFindFollowers set mock for method FindFollowers.
func (m *profileRepositoryMock) setFindFollowers(
	id uuid.UUID,
	profiles []domain.Profile,
	err error) {
	m.On("FindFollowers", id).
		Return(profiles, err)
}

// setFindFollowings set mock for method FindFollowings.
func (m *profileRepositoryMock) setFindFollowings(
	id uuid.UUID,
	profiles []domain.Profile,
	err error) {
	m.On("FindFollowings", id).
		Return(profiles, err)
}
