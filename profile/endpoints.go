package profile

import (
	"context"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth"

	"github.com/go-kit/kit/endpoint"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/types"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type Endpoints struct {
	ReadEndpoint   endpoint.Endpoint
	ListEndpoint   endpoint.Endpoint
	UpsertEndpoint endpoint.Endpoint

	AddFollowerEndpoint    endpoint.Endpoint
	DeleteFollowerEndpoint endpoint.Endpoint
	ListFollowingsEndpoint endpoint.Endpoint
	ListFollowersEndpoint  endpoint.Endpoint
}

// MakeServerEndpoints returns an Endpoints struct where each endpoint invokes
// the corresponding method on the provided service. Useful in a profilesvc
// server.
func MakeServerEndpoints(s Service, params auth.EndpointParams) Endpoints {
	authMiddleware := auth.AuthenticationMiddleware(params)

	return Endpoints{
		ReadEndpoint:   authMiddleware(MakeReadEndpoint(s)),
		ListEndpoint:   authMiddleware(MakeListEndpoint(s)),
		UpsertEndpoint: authMiddleware(MakeUpsertEndpoint(s)),

		AddFollowerEndpoint:    authMiddleware(MakeAddFollowerEndpoint(s)),
		DeleteFollowerEndpoint: authMiddleware(MakeDeleteFollowerEndpoint(s)),
		ListFollowingsEndpoint: authMiddleware(MakeListFollowingsEndpoint(s)),
		ListFollowersEndpoint:  authMiddleware(MakeListFollowersEndpoint(s)),
	}
}

// readRequest collects the request parameters for the Read method.
type readRequest struct {
	ID uuid.UUID
}

// readResponse  collects the response parameters for the Read method.
type readResponse struct {
	*domain.Profile
	Err error `json:"err,omitempty"`
}

// Failed implements Failer.
//kitgen:autogenerate
func (r readResponse) Failed() error {
	return r.Err
}

// MakeReadEndpoint returns an endpoint that invokes Read on the service.
//kitgen:autogenerate
func MakeReadEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(readRequest)
		profile, err := s.Read(ctx, req.ID)
		return readResponse{
			Profile: profile,
			Err:     err,
		}, nil
	}
}

// listRequest collects the request parameters for the List method.
type listRequest struct {
	FirstName string `json:"-"`
	LastName  string `json:"-"`
	Limit     int    `json:"-"`
	Offset    int64  `json:"-"`
}

// listResponse collects the response parameters for the List method.
type listResponse struct {
	Profiles []domain.Profile `json:"data"`
	Err      error            `json:"err,omitempty"`
}

// Failed implements Failer.
func (r listResponse) Failed() error {
	return r.Err
}

// MakeListEndpoint returns an endpoint that invokes List on the service.
func MakeListEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listRequest)
		profiles, err := s.List(ctx, req.Limit, req.Offset, filter{
			FirstName: req.FirstName,
			LastName:  req.LastName,
		})
		return listResponse{
			Profiles: profiles,
			Err:      err,
		}, nil
	}
}

type upsertProfileRequest struct {
	ID        uuid.UUID         `json:"-"`
	FirstName string            `json:"first_name"`
	LastName  string            `json:"last_name"`
	BirthDate *types.Date       `json:"birth_date"`
	Gender    domain.GenderType `json:"gender"`
	Interests string            `json:"interests"`
	City      string            `json:"city"`
}

type upsertProfileResponse struct {
	domain.Profile
	Err error `json:"err,omitempty"`
}

// Failed implements Failer.
func (r upsertProfileResponse) Failed() error {
	return r.Err
}

// MakeUpsertEndpoint returns an endpoint via the passed service.
func MakeUpsertEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(upsertProfileRequest)
		profile, err := s.Upsert(ctx, req.ID, domain.Profile{
			FirstName: req.FirstName,
			LastName:  req.LastName,
			BirthDate: req.BirthDate,
			Gender:    req.Gender,
			Interests: req.Interests,
			City:      req.City,
		})
		if err != nil {
			return nil, err
		}
		return upsertProfileResponse{
			Profile: *profile,
		}, nil
	}
}

// addFollowerRequest collects the request parameters for the AddFollower method.
type addFollowerRequest struct {
	ID         uuid.UUID
	FollowerID uuid.UUID
}

// addFollowerResponse  collects the response parameters for the AddFollower method.
type addFollowerResponse struct {
	Err error `json:"err,omitempty"`
}

// Failed implements Failer.
func (r addFollowerResponse) Failed() error {
	return r.Err
}

// MakeAddFollowerEndpoint returns an endpoint that invokes AddFollower on the service.
func MakeAddFollowerEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addFollowerRequest)
		err := s.AddFollower(ctx, req.ID, req.FollowerID)
		return addFollowerResponse{
			Err: err,
		}, nil
	}
}

// deleteFollowerRequest collects the request parameters for the AddFollower method.
type deleteFollowerRequest struct {
	ID         uuid.UUID
	FollowerID uuid.UUID
}

// deleteFollowerResponse  collects the response parameters for the AddFollower method.
type deleteFollowerResponse struct {
	Err error `json:"err,omitempty"`
}

// Failed implements Failer.
func (r deleteFollowerResponse) Failed() error {
	return r.Err
}

// MakeDeleteFollowerEndpoint returns an endpoint that invokes AddFollower on the service.
func MakeDeleteFollowerEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deleteFollowerRequest)
		err := s.RemoveFollower(ctx, req.ID, req.FollowerID)
		return deleteFollowerResponse{
			Err: err,
		}, nil
	}
}

// listFollowingsRequest collects the request parameters for the ListFollowings method.
type listFollowingsRequest struct {
	ID uuid.UUID
}

// listFollowingsResponse collects the response parameters for the ListFollowings method.
type listFollowingsResponse struct {
	Profiles []domain.Profile `json:"data"`
	Err      error            `json:"err,omitempty"`
}

// Failed implements Failer.
func (r listFollowingsResponse) Failed() error {
	return r.Err
}

// MakeListFollowingsEndpoint returns an endpoint that invokes ListFollowings on the service.
func MakeListFollowingsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listFollowingsRequest)
		profiles, err := s.ListFollowings(ctx, req.ID)
		return listFollowingsResponse{
			Profiles: profiles,
			Err:      err,
		}, nil
	}
}

// listFollowersRequest collects the request parameters for the ListFollowers method.
type listFollowersRequest struct {
	ID uuid.UUID
}

// listFollowersResponse collects the response parameters for the ListFollowers method.
type listFollowersResponse struct {
	Profiles []domain.Profile `json:"data"`
	Err      error            `json:"err,omitempty"`
}

// Failed implements Failer.
func (r listFollowersResponse) Failed() error {
	return r.Err
}

// MakeListFollowersEndpoint returns an endpoint that invokes ListFollowers on the service.
func MakeListFollowersEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listFollowersRequest)
		profiles, err := s.ListFollowers(ctx, req.ID)
		return listFollowersResponse{
			Profiles: profiles,
			Err:      err,
		}, nil
	}
}
