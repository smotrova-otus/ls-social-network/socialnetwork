package profile

import (
	"github.com/stretchr/testify/mock"
)

// passwordHasherMock is a mock implementation of PasswordHashService interface.
type passwordHasherMock struct {
	mock.Mock
}

// newPasswordHasherMock is a constructor.
func newPasswordHasherMock() *passwordHasherMock {
	return &passwordHasherMock{}
}

// CreateHash implementation.
func (m *passwordHasherMock) CreateHash(password, salt []byte) ([]byte, error) {
	results := m.Called(password, salt)

	return results.Get(0).([]byte), results.Error(1)
}

// MakeSalt implementation.
func (m *passwordHasherMock) MakeSalt() (salt []byte, err error) {
	results := m.Called()
	return results.Get(0).([]byte), results.Error(1)
}

// setCreateHashset mock for method CreateHash.
func (m *passwordHasherMock) setCreateHash(password, salt, hash []byte, err error) {
	m.On("CreateHash", password, salt).
		Return(hash, err)
}

// setMakeSalt mock for method MakeSalt.
func (m *passwordHasherMock) setMakeSalt(salt []byte, err error) {
	m.On("MakeSalt").
		Return(salt, err)
}
