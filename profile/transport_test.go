package profile

import (
	"context"
	"errors"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	pkgerrors "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

// TestRouter tests that all routes are bound to handlers.
func TestRouter(t *testing.T) {
	var ep = func(
		ctx context.Context,
		request interface{},
	) (response interface{}, err error) {
		return nil, nil
	}

	_, logger := log.Default()
	tests := []struct {
		name   string
		method string
		url    string
	}{
		{
			name:   "GET_/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3",
			url:    "/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3",
			method: http.MethodGet,
		},
		{
			name:   "PUT_/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3",
			url:    "/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3",
			method: http.MethodPut,
		},
		{
			name:   "GET_/v0/profiles",
			url:    "/v0/profiles",
			method: http.MethodGet,
		},
		{
			name:   "GET_/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3/followers",
			url:    "/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3/followers",
			method: http.MethodGet,
		},
		{
			name:   "GET_/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3/followings",
			url:    "/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3/followings",
			method: http.MethodGet,
		},
		{
			name:   "GET_/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3/followers/7d2838fc-cbf8-4553-a671-474c591bcac1",
			url:    "/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3/followers/7d2838fc-cbf8-4553-a671-474c591bcac1",
			method: http.MethodDelete,
		},
		{
			name:   "PUT_/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3/followers/7d2838fc-cbf8-4553-a671-474c591bcac1",
			url:    "/v0/profiles/7d2838fc-cbf8-4553-a671-474c591bcac3/followers/7d2838fc-cbf8-4553-a671-474c591bcac1",
			method: http.MethodPut,
		},
	}
	router := mux.NewRouter()

	handlerSet := NewHTTPHandlerSet(
		Endpoints{
			ReadEndpoint:           ep,
			ListEndpoint:           ep,
			UpsertEndpoint:         ep,
			AddFollowerEndpoint:    ep,
			DeleteFollowerEndpoint: ep,
			ListFollowingsEndpoint: ep,
			ListFollowersEndpoint:  ep,
		},
		HTTPHandlerParams{
			Logger: logger,
		},
	)
	// All checked handlers must be added to router here.
	handlerSet.Register(router)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res := httptest.NewRecorder()
			req, _ := http.NewRequest(
				tt.method,
				tt.url,
				strings.NewReader("{}"),
			)
			router.ServeHTTP(res, req)

			assert.Equal(t, http.StatusOK, res.Code)
		})
	}
}

func TestReadHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeReadProfileHandler(Endpoints{
		ReadEndpoint: MakeReadEndpoint(serviceMock),
	}, getHTTPHandlerOptions("Read", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url     string
		muxVars map[string]string
	}

	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setRead(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					&domain.Profile{
						ID:        uuid.MustParse("00000000-0000-4000-8000-000000000000"),
						FirstName: "test first name",
						LastName:  "test last name",
						BirthDate: nil,
						Gender:    "male",
						Interests: "interests",
						City:      "city",
					},
					nil,
				)
			},
			args: args{
				url:     "/v0/profiles/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
			},
			wantCode: http.StatusOK,
			wantBody: `{"id":"00000000-0000-4000-8000-000000000000","first_name":"test first name","last_name":"test last name","birth_date":null,"gender":"male","interests":"interests","city":"city"}` + "\n",
		},
		{
			name:  "invalid profile id",
			setup: func() {},
			args: args{
				url:     "/v0/profiles/invalid_uuid",
				muxVars: map[string]string{"id": "invalid_uuid"},
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid path parameters","code":"INVALID_PATH_PARAMETERS"}`,
		},
		{
			name: "profile not found err",
			setup: func() {
				serviceMock.setRead(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					nil,
					pkgerrors.ErrNotFound,
				)
			},
			args: args{
				url:     "/v0/profiles/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
			},
			wantCode: http.StatusNotFound,
			wantBody: `{"error":"not found","code":"NOT_FOUND"}`,
		},
		{
			name: "general service err",
			setup: func() {
				serviceMock.setRead(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					nil,
					errors.New("general error"),
				)
			},
			args: args{
				url:     "/v0/profiles/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodGet,
				tt.args.url,
				nil,
			)
			if err != nil {
				t.Fatal(err)
			}
			req = mux.SetURLVars(req, tt.args.muxVars)
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestUpsertHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeUpsertProfileHandler(Endpoints{
		UpsertEndpoint: MakeUpsertEndpoint(serviceMock),
	}, getHTTPHandlerOptions("Upsert", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url     string
		muxVars map[string]string
		body    string
	}

	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setUpsert(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					domain.Profile{
						FirstName: "test first name",
						LastName:  "test last name",
						BirthDate: nil,
						Gender:    "male",
						Interests: "interests",
						City:      "city",
					},
					&domain.Profile{
						ID:        uuid.MustParse("00000000-0000-4000-8000-000000000000"),
						FirstName: "test first name",
						LastName:  "test last name",
						BirthDate: nil,
						Gender:    "male",
						Interests: "interests",
						City:      "city",
					},
					nil,
				)
			},
			args: args{
				url:     "/v0/profiles/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
				body:    `{"first_name":"test first name","last_name":"test last name","gender":"male","interests":"interests","city":"city"}`,
			},
			wantCode: http.StatusOK,
			wantBody: `{"id":"00000000-0000-4000-8000-000000000000","first_name":"test first name","last_name":"test last name","birth_date":null,"gender":"male","interests":"interests","city":"city"}` + "\n",
		},
		{
			name:  "invalid profile id",
			setup: func() {},
			args: args{
				url:     "/v0/profiles/invalid_uuid",
				muxVars: map[string]string{"id": "invalid_uuid"},
				body:    `{"first_name":"test first name","last_name":"test last name","gender":"male","interests":"interests","city":"city"}`,
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid path parameters","code":"INVALID_PATH_PARAMETERS"}`,
		},
		{
			name: "some service err",
			setup: func() {
				serviceMock.setUpsert(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					domain.Profile{
						FirstName: "test first name",
						LastName:  "test last name",
						BirthDate: nil,
						Gender:    "male",
						Interests: "interests",
						City:      "city",
					},
					nil,
					domain.ErrInvalidFirstNameLength,
				)
			},
			args: args{
				url:     "/v0/profiles/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
				body:    `{"first_name":"test first name","last_name":"test last name","gender":"male","interests":"interests","city":"city"}`,
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid first_name length","code":"BAD_REQUEST"}`,
		},
		{
			name: "internal service err",
			setup: func() {
				serviceMock.setUpsert(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					domain.Profile{
						FirstName: "test first name",
						LastName:  "test last name",
						BirthDate: nil,
						Gender:    "male",
						Interests: "interests",
						City:      "city",
					},
					nil,
					errors.New("general error"),
				)
			},
			args: args{
				url:     "/v0/profiles/00000000-0000-4000-8000-000000000000",
				muxVars: map[string]string{"id": "00000000-0000-4000-8000-000000000000"},
				body:    `{"first_name":"test first name","last_name":"test last name","gender":"male","interests":"interests","city":"city"}`,
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodPut,
				tt.args.url,
				strings.NewReader(tt.args.body),
			)
			if err != nil {
				t.Fatal(err)
			}
			req = mux.SetURLVars(req, tt.args.muxVars)
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestListHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeListProfileHandler(Endpoints{
		ListEndpoint: MakeListEndpoint(serviceMock),
	}, getHTTPHandlerOptions("List", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url string
	}
	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setList(1, 1, filter{
					FirstName: "testF",
					LastName:  "testL",
				}, []domain.Profile{
					{
						ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						FirstName: "testFirst name",
						LastName:  "testLast name",
						BirthDate: nil,
						Gender:    "male",
						Interests: "interests",
						City:      "city",
					},
				}, nil)
			},
			args: args{
				url: "/v0/profiles?lastName=testL&firstName=testF&limit=1&offset=1",
			},
			wantCode: http.StatusOK,
			wantBody: `{"data":[{"id":"7d2838fc-cbf8-4553-a671-474c591bcac3","first_name":"testFirst name","last_name":"testLast name","birth_date":null,"gender":"male","interests":"interests","city":"city"}]}` + "\n",
		},
		{
			name: "success_without_filter_and_offset",
			setup: func() {
				serviceMock.setList(0, 0, filter{
					LastName: "",
				}, []domain.Profile{
					{
						ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
						FirstName: "test first name",
						LastName:  "test last name",
						BirthDate: nil,
						Gender:    "male",
						Interests: "interests",
						City:      "city",
					},
				}, nil)
			},
			args: args{
				url: "/v0/profiles",
			},
			wantCode: http.StatusOK,
			wantBody: `{"data":[{"id":"7d2838fc-cbf8-4553-a671-474c591bcac3","first_name":"test first name","last_name":"test last name","birth_date":null,"gender":"male","interests":"interests","city":"city"}]}` + "\n",
		},
		{
			name: "service_fail",
			setup: func() {
				serviceMock.setList(1, 1, filter{}, []domain.Profile{}, errors.New("some err"))
			},
			args: args{
				url: "/v0/profiles?limit=1&offset=1",
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
		{
			name: "bad_limit",
			setup: func() {
			},
			args: args{
				url: "/v0/profiles?limit=asd",
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid query parameters","code":"INVALID_QUERY_PARAMETERS"}`,
		},
		{
			name: "bad_offset",
			setup: func() {
			},
			args: args{
				url: "/v0/profiles?offset=asd",
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid query parameters","code":"INVALID_QUERY_PARAMETERS"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodGet,
				tt.args.url,
				nil,
			)
			if err != nil {
				t.Fatal(err)
			}
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestAddFollowerHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeAddFollowerHandler(Endpoints{
		AddFollowerEndpoint: MakeAddFollowerEndpoint(serviceMock),
	}, getHTTPHandlerOptions("AddFollower", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url     string
		muxVars map[string]string
	}

	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setAddFollower(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					uuid.MustParse("00000000-0000-4000-8000-000000000001"),
					nil,
				)
			},
			args: args{
				url: "/v0/profiles/00000000-0000-4000-8000-000000000000/00000000-0000-4000-8000-000000000001",
				muxVars: map[string]string{
					"id":          "00000000-0000-4000-8000-000000000000",
					"follower_id": "00000000-0000-4000-8000-000000000001",
				},
			},
			wantCode: http.StatusOK,
			wantBody: `{}` + "\n",
		},
		{
			name:  "invalid profile id",
			setup: func() {},
			args: args{
				url:     "/v0/profiles/invalid_uuid",
				muxVars: map[string]string{"id": "invalid_uuid"},
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid path parameters","code":"INVALID_PATH_PARAMETERS"}`,
		},
		{
			name: "profile not found err",
			setup: func() {
				serviceMock.setAddFollower(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					uuid.MustParse("00000000-0000-4000-8000-000000000001"),
					pkgerrors.ErrNotFound,
				)
			},
			args: args{
				url: "/v0/profiles/00000000-0000-4000-8000-000000000000/00000000-0000-4000-8000-000000000001",
				muxVars: map[string]string{
					"id":          "00000000-0000-4000-8000-000000000000",
					"follower_id": "00000000-0000-4000-8000-000000000001",
				},
			},
			wantCode: http.StatusNotFound,
			wantBody: `{"error":"not found","code":"NOT_FOUND"}`,
		},
		{
			name: "general service err",
			setup: func() {
				serviceMock.setAddFollower(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					uuid.MustParse("00000000-0000-4000-8000-000000000001"),
					errors.New("general error"),
				)
			},
			args: args{
				url: "/v0/profiles/00000000-0000-4000-8000-000000000000/00000000-0000-4000-8000-000000000001",
				muxVars: map[string]string{
					"id":          "00000000-0000-4000-8000-000000000000",
					"follower_id": "00000000-0000-4000-8000-000000000001",
				},
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodPut,
				tt.args.url,
				nil,
			)
			if err != nil {
				t.Fatal(err)
			}
			req = mux.SetURLVars(req, tt.args.muxVars)
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestDeleteFollowerHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeDeleteFollowerHandler(Endpoints{
		DeleteFollowerEndpoint: MakeDeleteFollowerEndpoint(serviceMock),
	}, getHTTPHandlerOptions("DeleteFollower", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url     string
		muxVars map[string]string
	}

	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setRemoveFollower(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					uuid.MustParse("00000000-0000-4000-8000-000000000001"),
					nil,
				)
			},
			args: args{
				url: "/v0/profiles/00000000-0000-4000-8000-000000000000/00000000-0000-4000-8000-000000000001",
				muxVars: map[string]string{
					"id":          "00000000-0000-4000-8000-000000000000",
					"follower_id": "00000000-0000-4000-8000-000000000001",
				},
			},
			wantCode: http.StatusOK,
			wantBody: `{}` + "\n",
		},
		{
			name:  "invalid profile id",
			setup: func() {},
			args: args{
				url:     "/v0/profiles/invalid_uuid",
				muxVars: map[string]string{"id": "invalid_uuid"},
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid path parameters","code":"INVALID_PATH_PARAMETERS"}`,
		},
		{
			name: "profile not found err",
			setup: func() {
				serviceMock.setRemoveFollower(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					uuid.MustParse("00000000-0000-4000-8000-000000000001"),
					pkgerrors.ErrNotFound,
				)
			},
			args: args{
				url: "/v0/profiles/00000000-0000-4000-8000-000000000000/00000000-0000-4000-8000-000000000001",
				muxVars: map[string]string{
					"id":          "00000000-0000-4000-8000-000000000000",
					"follower_id": "00000000-0000-4000-8000-000000000001",
				},
			},
			wantCode: http.StatusNotFound,
			wantBody: `{"error":"not found","code":"NOT_FOUND"}`,
		},
		{
			name: "general service err",
			setup: func() {
				serviceMock.setRemoveFollower(
					uuid.MustParse("00000000-0000-4000-8000-000000000000"),
					uuid.MustParse("00000000-0000-4000-8000-000000000001"),
					errors.New("general error"),
				)
			},
			args: args{
				url: "/v0/profiles/00000000-0000-4000-8000-000000000000/00000000-0000-4000-8000-000000000001",
				muxVars: map[string]string{
					"id":          "00000000-0000-4000-8000-000000000000",
					"follower_id": "00000000-0000-4000-8000-000000000001",
				},
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodDelete,
				tt.args.url,
				nil,
			)
			if err != nil {
				t.Fatal(err)
			}
			req = mux.SetURLVars(req, tt.args.muxVars)
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestListFollowersHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeListFollowersHandler(Endpoints{
		ListFollowersEndpoint: MakeListFollowersEndpoint(serviceMock),
	}, getHTTPHandlerOptions("ListFollowers", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url     string
		muxVars map[string]string
	}
	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setListFollowers(uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					[]domain.Profile{
						{
							ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
							FirstName: "test first name",
							LastName:  "test last name",
							BirthDate: nil,
							Gender:    "male",
							Interests: "interests",
							City:      "city",
						},
					}, nil)
			},
			args: args{
				url: "/v0/profiles/d2838fc-cbf8-4553-a671-474c591bcac1/followers",
				muxVars: map[string]string{
					"id": "7d2838fc-cbf8-4553-a671-474c591bcac1",
				},
			},
			wantCode: http.StatusOK,
			wantBody: `{"data":[{"id":"7d2838fc-cbf8-4553-a671-474c591bcac3","first_name":"test first name","last_name":"test last name","birth_date":null,"gender":"male","interests":"interests","city":"city"}]}` + "\n",
		},
		{
			name:  "invalid profile id",
			setup: func() {},
			args: args{
				url:     "/v0/profiles/invalid_uuid/followers",
				muxVars: map[string]string{"id": "invalid_uuid"},
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid path parameters","code":"INVALID_PATH_PARAMETERS"}`,
		},
		{
			name: "service_fail",
			setup: func() {
				serviceMock.setListFollowers(uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					nil, errors.New("some err"))
			},
			args: args{
				url: "/v0/profiles/invalid_uuid/followers",
				muxVars: map[string]string{
					"id": "7d2838fc-cbf8-4553-a671-474c591bcac1",
				},
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodGet,
				tt.args.url,
				nil,
			)
			if err != nil {
				t.Fatal(err)
			}
			req = mux.SetURLVars(req, tt.args.muxVars)
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}

func TestListFollowingsHandler(t *testing.T) {
	_, logger := log.Default()
	serviceMock := newServiceMock()
	handler := makeListFollowingsHandler(Endpoints{
		ListFollowingsEndpoint: MakeListFollowingsEndpoint(serviceMock),
	}, getHTTPHandlerOptions("ListFollowings", HTTPHandlerParams{
		Logger: logger,
	}))

	type args struct {
		url     string
		muxVars map[string]string
	}
	tests := []struct {
		name     string
		args     args
		setup    func()
		wantCode int
		wantBody string
	}{
		{
			name: "success",
			setup: func() {
				serviceMock.setListFollowings(uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					[]domain.Profile{
						{
							ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
							FirstName: "test first name",
							LastName:  "test last name",
							BirthDate: nil,
							Gender:    "male",
							Interests: "interests",
							City:      "city",
						},
					}, nil)
			},
			args: args{
				url: "/v0/profiles/d2838fc-cbf8-4553-a671-474c591bcac1/Followings",
				muxVars: map[string]string{
					"id": "7d2838fc-cbf8-4553-a671-474c591bcac1",
				},
			},
			wantCode: http.StatusOK,
			wantBody: `{"data":[{"id":"7d2838fc-cbf8-4553-a671-474c591bcac3","first_name":"test first name","last_name":"test last name","birth_date":null,"gender":"male","interests":"interests","city":"city"}]}` + "\n",
		},
		{
			name:  "invalid profile id",
			setup: func() {},
			args: args{
				url:     "/v0/profiles/invalid_uuid/followings",
				muxVars: map[string]string{"id": "invalid_uuid"},
			},
			wantCode: http.StatusBadRequest,
			wantBody: `{"error":"invalid path parameters","code":"INVALID_PATH_PARAMETERS"}`,
		},
		{
			name: "service_fail",
			setup: func() {
				serviceMock.setListFollowings(uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					nil, errors.New("some err"))
			},
			args: args{
				url: "/v0/profiles/invalid_uuid/followings",
				muxVars: map[string]string{
					"id": "7d2838fc-cbf8-4553-a671-474c591bcac1",
				},
			},
			wantCode: http.StatusInternalServerError,
			wantBody: `{"error":"internal server error","code":"INTERNAL_SERVER_ERROR"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setup()
			res := httptest.NewRecorder()
			req, err := http.NewRequest(
				http.MethodGet,
				tt.args.url,
				nil,
			)
			if err != nil {
				t.Fatal(err)
			}
			req = mux.SetURLVars(req, tt.args.muxVars)
			handler.ServeHTTP(res, req)

			// Check current AssertExpectations and clear ExpectedCalls before next test.
			serviceMock.AssertExpectations(t)
			serviceMock.ExpectedCalls = nil

			assert.Equal(t, tt.wantCode, res.Code, "status code not valid")
			assert.Equal(t, tt.wantBody, res.Body.String())
		})
	}
}
