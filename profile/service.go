package profile

import (
	"context"
	"fmt"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
)

type Service interface {
	List(ctx context.Context, limit int, offset int64, filter filter) ([]domain.Profile, error)
	Upsert(ctx context.Context, id uuid.UUID, profile domain.Profile) (result *domain.Profile, err error)
	Read(ctx context.Context, id uuid.UUID) (result *domain.Profile, err error)
	AddFollower(ctx context.Context, id, followerID uuid.UUID) error
	RemoveFollower(ctx context.Context, id, followerID uuid.UUID) error
	ListFollowings(ctx context.Context, id uuid.UUID) ([]domain.Profile, error)
	ListFollowers(ctx context.Context, id uuid.UUID) ([]domain.Profile, error)
}

type service struct {
	repository    Repository
	uuidGenerator uuid.Generator
}

func (s service) Upsert(ctx context.Context, id uuid.UUID, profileFields domain.Profile) (result *domain.Profile, err error) {
	logger := log.Must(ctx)
	logger.WithField("method", "Upsert")
	currentUser, err := auth.CurrentUser(ctx)
	if err != nil {
		logger.
			WithError(err).
			Errorf("upsert profile failed: get current user failed (id: %v)", id)
		return nil, err
	}
	if currentUser == nil {
		logger.
			WithError(err).
			Warnf("upsert profile failed: access denied (id: %v)", id)
		return nil, errors.ErrUnauthorized
	}
	if currentUser.ID != id {
		logger.
			Warnf("upsert profile failed: access denied (id: %v)", id)
		return nil, errors.ErrForbidden
	}
	profileFields.Modify()
	if err = profileFields.Validate(); err != nil {
		logger.
			WithError(err).
			Warnf("upsert profile failed: validation failed (id: %v)", id)
		return nil, err
	}
	profile, err := s.repository.FindByID(id)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("upsert profile failed: find profile in storage error (id: %v)", id)
		return nil, fmt.Errorf("find profile in storage error: %w", err)
	}

	if profile == nil {
		profile = &domain.Profile{
			ID:        id,
			FirstName: profileFields.FirstName,
			LastName:  profileFields.LastName,
			BirthDate: profileFields.BirthDate,
			Gender:    profileFields.Gender,
			Interests: profileFields.Interests,
			City:      profileFields.City,
		}
		err = s.repository.Insert(profile)
		if err != nil {
			logger.
				WithField("err", err).
				Errorf("upsert profile failed: insert profile in storage error (id: %v)", id)
			return nil, fmt.Errorf("update profile in storage error: %w", err)
		}
		logger.
			Infof("upsert profile succeeded: profile was not found, profile is being created now (id: %v)", id)
		return profile, nil
	}
	profile.FirstName = profileFields.FirstName
	profile.LastName = profileFields.LastName
	profile.City = profileFields.City
	profile.BirthDate = profileFields.BirthDate
	profile.Interests = profileFields.Interests
	profile.Gender = profileFields.Gender
	err = s.repository.Update(profile)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("upsert profile failed: update in storage error (id: %v)", id)
		return nil, fmt.Errorf("update profile in storage error: %w", err)
	}
	logger.
		Infof("upsert profile succeeded: profile has already existed, it is being updated now  (id: %v)", id)
	return profile, nil
}

func (s service) AddFollower(ctx context.Context, followingProfileID, followerID uuid.UUID) error {
	logger := log.Must(ctx)
	logger.WithField("method", "AddFollower")
	currentUser, err := auth.CurrentUser(ctx)
	if err != nil {
		logger.
			WithError(err).
			Errorf("add follower failed: get current user failed (followerId: %v, following: %v)", followerID, followingProfileID)
		return err
	}
	if currentUser == nil {
		logger.
			WithError(err).
			Warnf("add follower failed:: access denied (followerId: %v, following: %v)", followerID, followingProfileID)
		return errors.ErrUnauthorized
	}
	if currentUser.ID != followerID {
		logger.
			Warnf("add follower failed:: access denied (followerId: %v, following: %v)", followerID, followingProfileID)
		return errors.ErrForbidden
	}
	if followingProfileID == followerID {
		logger.
			Warnf("add follower failed: followerId=following %v (followerId: %v, following: %v)", followerID, followerID, followingProfileID)
		return errors.ErrBadRequest
	}
	profile, err := s.repository.FindByID(followerID)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("add follower failed: find profile in storage error (followerId: %v, following: %v)", followerID, followingProfileID)
		return fmt.Errorf("find profile %v in storage error: %w", followerID, err)
	}
	if profile == nil {
		logger.
			Warnf("add following failed: not found profile %v (followerId: %v, following: %v)", followerID, followerID, followingProfileID)
		return errors.ErrNotFound
	}
	followingProfile, err := s.repository.FindByID(followingProfileID)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("add following failed: find profile in storage error (followerId: %v, following: %v)", followerID, followingProfileID)
		return fmt.Errorf("find profile %v in storage error: %w", followingProfileID, err)
	}
	if followingProfile == nil {
		logger.
			Warnf("add following failed: not found following profile %v (followerId: %v, following: %v)", followingProfileID, followerID, followingProfileID)
		return errors.ErrNotFound
	}

	err = s.repository.AddFollower(followingProfileID, followerID)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("add follower failed: "+
				"add follower in storage failed (followerId: %v, following: %v)", followerID, followingProfileID)
		return fmt.Errorf("add follower %v to %v in storage failed: %w", followerID, followingProfileID, err)
	}

	logger.
		Infof("add follower succeeded (followerId: %v, following: %v)", followerID, followingProfileID)
	return nil
}

func (s service) RemoveFollower(ctx context.Context, followingProfileID, followerID uuid.UUID) error {
	logger := log.Must(ctx)
	logger.WithField("method", "RemoveFollower")
	currentUser, err := auth.CurrentUser(ctx)
	if err != nil {
		logger.
			WithError(err).
			Errorf("remove follower failed: get current user failed (followerId: %v, following: %v)", followerID, followingProfileID)
		return err
	}
	if currentUser == nil {
		logger.
			WithError(err).
			Warnf("remove follower failed:: access denied (followerId: %v, following: %v)", followerID, followingProfileID)
		return errors.ErrUnauthorized
	}
	if currentUser.ID != followerID {
		logger.
			Warnf("remove follower failed:: access denied (followerId: %v, following: %v)", followerID, followingProfileID)
		return errors.ErrForbidden
	}
	profile, err := s.repository.FindByID(followerID)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("remove follower failed: find profile in storage error (followerId: %v, following: %v)", followerID, followingProfileID)
		return fmt.Errorf("find profile %v in storage error: %w", followerID, err)
	}
	if profile == nil {
		logger.
			Warnf("remove follower failed: not found profile %v (followerId: %v, following: %v)", followerID, followerID, followingProfileID)
		return errors.ErrNotFound
	}
	followingProfile, err := s.repository.FindByID(followingProfileID)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("remove follower failed: find profile in storage error (followerId: %v, following: %v)", followerID, followingProfileID)
		return fmt.Errorf("find profile %v in storage error: %w", followingProfileID, err)
	}
	if followingProfile == nil {
		logger.
			Warnf("remove follower failed: not found following profile %v (followerId: %v, following: %v)", followingProfileID, followerID, followingProfileID)
		return errors.ErrNotFound
	}

	err = s.repository.DeleteFollower(followingProfileID, followerID)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("remove follower failed: "+
				"delete follower in storage failed (followerId: %v, following: %v)", followerID, followingProfileID)
		return fmt.Errorf("delete follower %v to %v in storage failed: %w", followerID, followingProfileID, err)
	}

	logger.
		Infof("remove follower succeeded (followerId: %v, following: %v)", followerID, followingProfileID)
	return nil
}

func (s service) ListFollowings(ctx context.Context, id uuid.UUID) ([]domain.Profile, error) {
	logger := log.Must(ctx)
	logger.WithField("method", "ListFollowings")
	profiles, err := s.repository.FindFollowings(id)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("list followings failed: (profileId: %v)", id)
		return nil, fmt.Errorf("list followings for %v failed %w", id, err)
	}
	logger.
		Infof("list followings succeeded (profileId: %v)", id)
	return profiles, nil
}

func (s service) ListFollowers(ctx context.Context, id uuid.UUID) ([]domain.Profile, error) {
	logger := log.Must(ctx)
	logger.WithField("method", "ListFollowers")
	profiles, err := s.repository.FindFollowers(id)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("list followers failed: (profileId: %v)", id)
		return nil, fmt.Errorf("list followers for %v failed %w", id, err)
	}
	logger.
		Infof("list followers succeeded (profileId: %v)", id)
	return profiles, nil
}

func (s service) Read(ctx context.Context, id uuid.UUID) (result *domain.Profile, err error) {
	logger := log.Must(ctx)
	logger.WithField("method", "Read")
	profile, err := s.repository.FindByID(id)
	if err != nil {
		logger.
			WithField("err", err).
			Errorf("read profile failed: find profile in storage error (id: %v)", id)
		return nil, fmt.Errorf("find profile in storage error: %w", err)
	}
	if profile == nil {
		logger.
			Warnf("read profile failed: profile not found (id: %v)", id)
		return nil, errors.ErrNotFound
	}

	logger.
		Infof("read profile succeeded (id: %v)", id)
	return profile, nil
}

// List returns list of user profiles.
func (s *service) List(
	ctx context.Context, limit int, offset int64, filter filter,
) ([]domain.Profile, error) {
	logger := log.Must(ctx)
	logger.WithField("method", "List")
	if limit == 0 {
		limit = -1
	}
	profiles, err := s.repository.FindAllWithFilter(
		offset,
		limit,
		filter,
	)
	if err != nil {
		logger.WithError(err).Error("get profiles list failed: get list from repository error")
		return nil, err
	}
	logger.Info("get profiles list succeeded")
	return profiles, nil
}

func NewService(
	repository Repository,
	uuidGenerator uuid.Generator,
) Service {
	return &service{
		repository:    repository,
		uuidGenerator: uuidGenerator,
	}
}
