package profile

import (
	"context"
	"encoding/json"
	"github.com/go-kit/kit/auth/jwt"
	kitendpoint "github.com/go-kit/kit/endpoint"
	kitlogrus "github.com/go-kit/kit/log/logrus"
	"github.com/go-kit/kit/transport"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	pkgerrors "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
	pkghttp "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/http"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"net/http"
	"strconv"
	"strings"

	kithttp "github.com/go-kit/kit/transport/http"
)

// HTTPHandlerParams provides params for handlers service options.
type HTTPHandlerParams struct {
	Logger *logrus.Entry
}

func getHTTPHandlerOptions(name string, params HTTPHandlerParams) []kithttp.ServerOption {
	logger := kitlogrus.NewLogrusLogger(params.Logger)

	return []kithttp.ServerOption{
		kithttp.ServerBefore(log.NewLoggerRequestMiddleware(params.Logger)),
		kithttp.ServerBefore(jwt.HTTPToContext()),
		kithttp.ServerErrorEncoder(pkghttp.ErrorEncoder),
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	}
}

// HTTPHandlerSet contains all service http handlers to register it in mux.router later.
type HTTPHandlerSet struct {
	UpsertProfileHandler http.Handler
	ReadProfileHandler   http.Handler
	ListProfileHandler   http.Handler

	ListFollowingsHandler http.Handler
	AddFollowerHandler    http.Handler
	DeleteFollowerHandler http.Handler

	ListFollowersHandler http.Handler
}

// NewHTTPHandlerSet returns a http handler set for registration in the mux.Router later
func NewHTTPHandlerSet(endpoints Endpoints, params HTTPHandlerParams) HTTPHandlerSet {
	return HTTPHandlerSet{
		ReadProfileHandler: makeReadProfileHandler(
			endpoints,
			getHTTPHandlerOptions("Read", params),
		),
		ListProfileHandler: makeListProfileHandler(
			endpoints,
			getHTTPHandlerOptions("List", params),
		),
		UpsertProfileHandler: makeUpsertProfileHandler(
			endpoints,
			getHTTPHandlerOptions("Upsert", params),
		),
		ListFollowingsHandler: makeListFollowingsHandler(
			endpoints,
			getHTTPHandlerOptions("ListFollowings", params),
		),
		AddFollowerHandler: makeAddFollowerHandler(
			endpoints,
			getHTTPHandlerOptions("AddFollower", params),
		),
		DeleteFollowerHandler: makeDeleteFollowerHandler(
			endpoints,
			getHTTPHandlerOptions("DeleteFollower", params),
		),
		ListFollowersHandler: makeListFollowersHandler(
			endpoints,
			getHTTPHandlerOptions("ListFollowers", params),
		),
	}
}

// Register add handlers to mux.Router with theirs paths.
func (h *HTTPHandlerSet) Register(m *mux.Router) {
	m.Methods("GET").Path("/v0/profiles").Handler(h.ListProfileHandler)
	m.Methods("GET").Path("/v0/profiles/{id}").Handler(h.ReadProfileHandler)
	m.Methods("PUT").Path("/v0/profiles/{id}").Handler(h.UpsertProfileHandler)

	m.Methods(http.MethodPut).
		Path("/v0/profiles/{id}/followers/{follower_id}").
		Handler(h.AddFollowerHandler)
	m.Methods(http.MethodDelete).
		Path("/v0/profiles/{id}/followers/{follower_id}").
		Handler(h.DeleteFollowerHandler)
	m.Methods(http.MethodGet).
		Path("/v0/profiles/{id}/followings").
		Handler(h.ListFollowingsHandler)
	m.Methods(http.MethodGet).
		Path("/v0/profiles/{id}/followers").
		Handler(h.ListFollowersHandler)
}

func decodeAddFollowerRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	followerID, err := uuid.Parse(vars["follower_id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	return addFollowerRequest{
		ID:         id,
		FollowerID: followerID,
	}, nil
}

func makeAddFollowerHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.AddFollowerEndpoint,
		decodeAddFollowerRequest,
		encodeResponse,
		options...,
	)
}

func decodeDeleteFollowerRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	followerID, err := uuid.Parse(vars["follower_id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	return deleteFollowerRequest{
		ID:         id,
		FollowerID: followerID,
	}, nil
}

func makeDeleteFollowerHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.DeleteFollowerEndpoint,
		decodeDeleteFollowerRequest,
		encodeResponse,
		options...,
	)
}

func decodeListFollowingsRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	return listFollowingsRequest{
		ID: id,
	}, nil
}

func makeListFollowingsHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.ListFollowingsEndpoint,
		decodeListFollowingsRequest,
		encodeResponse,
		options...,
	)
}

func decodeListFollowersRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	return listFollowersRequest{
		ID: id,
	}, nil
}

func makeListFollowersHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.ListFollowersEndpoint,
		decodeListFollowersRequest,
		encodeResponse,
		options...,
	)
}

func makeReadProfileHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.ReadEndpoint,
		decodeReadProfileRequest,
		encodeResponse,
		options...,
	)
}

func makeUpsertProfileHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.UpsertEndpoint,
		decodeUpsertProfileRequest,
		encodeResponse,
		options...,
	)
}

func makeListProfileHandler(endpoints Endpoints, options []kithttp.ServerOption) http.Handler {
	return kithttp.NewServer(
		endpoints.ListEndpoint,
		decodeListProfileRequest,
		encodeResponse,
		options...,
	)
}

func decodeReadProfileRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	return readRequest{ID: id}, nil
}

func decodeUpsertProfileRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req upsertProfileRequest
	if e := json.NewDecoder(r.Body).Decode(&req); e != nil {
		return nil, pkgerrors.ErrInvalidBody
	}
	vars := mux.Vars(r)
	req.ID, err = uuid.Parse(vars["id"])
	if err != nil {
		return nil, pkgerrors.ErrInvalidPathParameters
	}
	return req, nil
}

func decodeListProfileRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	req := listRequest{}
	queryVals := r.URL.Query()
	req.LastName = strings.TrimSpace(queryVals.Get("lastName"))
	req.FirstName = strings.TrimSpace(queryVals.Get("firstName"))
	limitStr := queryVals.Get("limit")

	if limitStr != "" {
		limit, err := strconv.Atoi(limitStr)
		if err != nil {
			return nil, pkgerrors.ErrInvalidQueryParameters
		}
		req.Limit = limit
	}

	offsetStr := queryVals.Get("offset")
	if offsetStr != "" {
		offset, err := strconv.Atoi(offsetStr)
		if err != nil {
			return nil, pkgerrors.ErrInvalidQueryParameters
		}
		req.Offset = int64(offset)
	}
	return req, nil
}

// encodeResponse is the common method to encode all response types to the
// client. I chose to do it this way because, since we're using JSON, there's no
// reason to provide anything more specific. It's certainly possible to
// specialize on a per-response (per-method) basis.
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(kitendpoint.Failer); ok && f.Failed() != nil {
		pkghttp.ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	if headerer, ok := response.(kithttp.Headerer); ok {
		for k, values := range headerer.Headers() {
			for _, v := range values {
				w.Header().Add(k, v)
			}
		}
	}
	code := http.StatusOK
	if sc, ok := response.(kithttp.StatusCoder); ok {
		code = sc.StatusCode()
	}
	w.WriteHeader(code)
	return kithttp.EncodeJSONResponse(ctx, w, response)
}
