package profile

import "github.com/google/wire"

var WireSet = wire.NewSet(
	NewService,
	NewHTTPHandlerSet,
	MakeServerEndpoints,
	wire.Bind(new(Repository), new(*RepositoryImpl)),
	NewProfileRepository,
	wire.Struct(new(HTTPHandlerParams), "*"),
)
