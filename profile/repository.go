package profile

import (
	"database/sql"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/database"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"strings"
)

type Repository interface {
	FindByID(id uuid.UUID) (*domain.Profile, error)
	AddFollower(profileID, followerID uuid.UUID) error
	DeleteFollower(profileID, followerID uuid.UUID) error
	Insert(profile *domain.Profile) error
	Update(profile *domain.Profile) error
	FindAllWithFilter(offset int64, limit int, f filter) ([]domain.Profile, error)
	FindFollowers(id uuid.UUID) ([]domain.Profile, error)
	FindFollowings(id uuid.UUID) ([]domain.Profile, error)
}

// repository represent profile db profile Repository.
type RepositoryImpl struct {
	masterConn database.QueryerExecer
	slaveConn  database.Queryer
}

// NewProfileRepository is constructor.
func NewProfileRepository(
	masterConn database.QueryerExecer,
	slaveConn database.Queryer,
) *RepositoryImpl {
	return &RepositoryImpl{
		masterConn: masterConn,
		slaveConn:  slaveConn,
	}
}

// FindAllWithFilter finds profiles by filter.
func (r RepositoryImpl) FindAllWithFilter(offset int64, limit int, f filter) ([]domain.Profile, error) {
	profiles := make([]domain.Profile, 0)
	var err error
	var args []interface{}
	query := "select * from profiles"
	var queryFilter []string
	if f.FirstName != "" {
		queryFilter = append(queryFilter, "first_name like ?")
		args = append(args, f.FirstName+"%")
	}
	if f.LastName != "" {
		queryFilter = append(queryFilter, "last_name like ?")
		args = append(args, f.LastName+"%")
	}
	if len(queryFilter) > 0 {
		query = query + " where " + strings.Join(queryFilter, " AND ")
	}
	query = query + " order by id desc"
	if limit != -1 {
		query = query + " limit ?, ?"
		args = append(args, offset, limit)
	}
	err = sqlx.Select(r.slaveConn, &profiles, query, args...)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return profiles, nil
}

// FindByID finds profile by id.
func (r RepositoryImpl) FindByID(id uuid.UUID) (profile *domain.Profile, err error) {
	profile = &domain.Profile{}
	err = sqlx.Get(r.masterConn, profile, "select * from profiles where id=? limit 1", id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return profile, nil
}

func (r RepositoryImpl) Insert(profile *domain.Profile) error {
	_, err := r.masterConn.Exec(
		"insert into profiles (id, first_name, last_name, city, interests, gender, birth_date) values(?,?,?,?,?,?,?)",
		profile.ID,
		profile.FirstName,
		profile.LastName,
		profile.City,
		profile.Interests,
		profile.Gender,
		profile.BirthDate,
	)
	if err != nil {
		return err
	}
	return nil
}

func (r RepositoryImpl) Update(profile *domain.Profile) error {
	res, err := r.masterConn.Exec(
		"update profiles set first_name=?, last_name=?, city=?, interests=?, gender=?, birth_date=? where id=?",
		profile.FirstName,
		profile.LastName,
		profile.City,
		profile.Interests,
		profile.Gender,
		profile.BirthDate,
		profile.ID,
	)
	if err != nil {
		return err
	}
	_, err = res.RowsAffected()
	if err != nil {
		return err
	}
	return nil
}

func (r RepositoryImpl) AddFollower(profileID, followerID uuid.UUID) error {
	_, err := r.masterConn.Exec(
		"insert ignore into followers set profile_id=?, follower_id=?",
		profileID,
		followerID,
	)
	if err != nil {
		return err
	}
	return nil
}

func (r RepositoryImpl) DeleteFollower(profileID, followerID uuid.UUID) error {
	_, err := r.masterConn.Exec(
		"delete from followers where profile_id=? and follower_id=?",
		profileID,
		followerID,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil
		}
		return err
	}
	return nil
}

func (r RepositoryImpl) FindFollowers(id uuid.UUID) ([]domain.Profile, error) {
	profiles := make([]domain.Profile, 0)
	err := sqlx.Select(r.masterConn, &profiles, "select p.* from profiles p "+
		"inner join followers f on f.follower_id=p.id "+
		"where f.profile_id=?", id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return profiles, nil
}

func (r RepositoryImpl) FindFollowings(id uuid.UUID) ([]domain.Profile, error) {
	profiles := make([]domain.Profile, 0)
	err := sqlx.Select(r.slaveConn, &profiles, "select p.* from profiles p "+
		"inner join followers f on f.profile_id=p.id "+
		"where f.follower_id=?", id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return profiles, nil
}
