package profile

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/require"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"os"
	"testing"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/database"
)

var db *sqlx.DB

func Test_profileRepository_FindByID(t *testing.T) {
	type args struct {
		id uuid.UUID
	}
	tests := []struct {
		name        string
		args        args
		wantProfile *domain.Profile
		wantErr     error
		setup       func(execer database.QueryerExecer)
	}{
		{
			name: "exists",
			args: args{
				id: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
			},
			wantProfile: &domain.Profile{
				ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
				FirstName: "test first name",
				LastName:  "test last name",
				City:      "city",
				Interests: "interests",
				Gender:    "male",
				BirthDate: nil,
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
		{
			name: "not_exists",
			args: args{
				id: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
			},
			wantProfile: nil,
			wantErr:     nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test first name', 'test last name', 'test city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewProfileRepository(tx, tx)
			gotProfile, err := r.FindByID(tt.args.id)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.wantProfile, gotProfile)
		})
	}
}

func Test_profileRepository_Insert(t *testing.T) {
	type args struct {
		profile *domain.Profile
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		setup   func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				profile: &domain.Profile{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
					FirstName: "test first name",
					LastName:  "test last name",
					City:      "city",
					Interests: "interests",
					Gender:    "male",
					BirthDate: nil,
				},
			},
			wantErr: false,
			setup: func(execer database.QueryerExecer) {

			},
		},
		{
			name: "error",
			args: args{
				profile: &domain.Profile{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					FirstName: "test first name",
					LastName:  "test last name",
					City:      "city",
					Interests: "interests",
					Gender:    "male",
					BirthDate: nil,
				},
			},
			wantErr: true,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewProfileRepository(tx, tx)
			err = r.Insert(tt.args.profile)
			require.Equal(t, tt.wantErr, err != nil, fmt.Sprintf("error existing is not equal actualErr=%v", err))
			if err != nil {
				return
			}

			profile := &domain.Profile{}
			err = sqlx.Get(r.masterConn, profile, "select * from profiles limit 1")

			require.NoError(t, err)
			require.Equal(t, profile, tt.args.profile)
		})
	}
}

func Test_profileRepository_Update(t *testing.T) {
	type args struct {
		profile *domain.Profile
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
		setup   func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				profile: &domain.Profile{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					FirstName: "test first name1",
					LastName:  "test last name1",
					City:      "city1",
					Interests: "interests1",
					Gender:    "female",
					BirthDate: nil,
				},
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewProfileRepository(tx, tx)
			err = r.Update(tt.args.profile)
			require.Equal(t, tt.wantErr, err)
			if err != nil {
				return
			}

			profile := &domain.Profile{}
			err = sqlx.Get(r.masterConn, profile, "select * from profiles limit 1")

			require.NoError(t, err)
			require.Equal(t, profile, tt.args.profile)
		})
	}
}

func Test_profileRepository_FindAllWithFilter(t *testing.T) {
	type args struct {
		filter filter
	}
	tests := []struct {
		name         string
		args         args
		wantProfiles []domain.Profile
		wantErr      error
		setup        func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				filter: filter{
					FirstName: "first",
					LastName:  "last",
				},
			},
			wantProfiles: []domain.Profile{
				{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					FirstName: "first name",
					LastName:  "last name",
					City:      "city",
					Interests: "interests",
					Gender:    "male",
					BirthDate: nil,
				},
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'first name', 'last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewProfileRepository(tx, tx)
			gotProfiles, err := r.FindAllWithFilter(0, -1, tt.args.filter)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.wantProfiles, gotProfiles)
		})
	}
}

func Test_profileRepository_AddFollower(t *testing.T) {
	type args struct {
		profileID  uuid.UUID
		followerID uuid.UUID
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		setup   func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				profileID:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: false,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac2', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
		{
			name: "exists",
			args: args{
				profileID:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: false,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac2', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO followers (profile_id, follower_id)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac2', '7d2838fc-cbf8-4553-a671-474c591bcac3');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewProfileRepository(tx, tx)
			err = r.AddFollower(tt.args.profileID, tt.args.followerID)
			require.Equal(t, tt.wantErr, err != nil, fmt.Sprintf("error existing is not equal actualErr=%v", err))
			if err != nil {
				return
			}

			data := struct {
				ProfileID  uuid.UUID `db:"profile_id"`
				FollowerID uuid.UUID `db:"follower_id"`
			}{}
			err = sqlx.Get(r.masterConn, &data, "select * from followers limit 1")

			require.NoError(t, err)
			require.Equal(t, data.ProfileID, tt.args.profileID)
			require.Equal(t, data.FollowerID, tt.args.followerID)
		})
	}
}

func Test_profileRepository_DeleteFollower(t *testing.T) {
	type args struct {
		profileID  uuid.UUID
		followerID uuid.UUID
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		setup   func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				profileID:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: false,
			setup: func(execer database.QueryerExecer) {

			},
		},
		{
			name: "exists",
			args: args{
				profileID:  uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac2"),
				followerID: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantErr: false,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac2', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO followers (profile_id, follower_id)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac2', '7d2838fc-cbf8-4553-a671-474c591bcac3');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewProfileRepository(tx, tx)
			err = r.DeleteFollower(tt.args.profileID, tt.args.followerID)
			require.Equal(t, tt.wantErr, err != nil, fmt.Sprintf("error existing is not equal actualErr=%v", err))
			if err != nil {
				return
			}

			data := &struct {
				ProfileID  uuid.UUID `db:"profile_id"`
				FollowerID uuid.UUID `db:"follower_id"`
			}{}
			err = sqlx.Get(r.masterConn, data, "select * from followers limit 1")

			require.Equal(t, sql.ErrNoRows, err)
		})
	}
}

func Test_profileRepository_FindFollowers(t *testing.T) {
	type args struct {
		id uuid.UUID
	}
	tests := []struct {
		name         string
		args         args
		wantProfiles []domain.Profile
		wantErr      error
		setup        func(execer database.QueryerExecer)
	}{
		{
			name: "success",
			args: args{
				id: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantProfiles: []domain.Profile{
				{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					FirstName: "test first name",
					LastName:  "test last name",
					City:      "city",
					Interests: "interests",
					Gender:    "male",
					BirthDate: nil,
				},
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}

				_, err = execer.Exec(`INSERT INTO followers (profile_id, follower_id)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', '7d2838fc-cbf8-4553-a671-474c591bcac1');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
		{
			name: "empty",
			args: args{
				id: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantProfiles: []domain.Profile{},
			wantErr:      nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}

				_, err = execer.Exec(`INSERT INTO followers (profile_id, follower_id)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', '7d2838fc-cbf8-4553-a671-474c591bcac3');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewProfileRepository(tx, tx)
			gotProfiles, err := r.FindFollowers(tt.args.id)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.wantProfiles, gotProfiles)
		})
	}
}

func Test_profileRepository_FindFollowings(t *testing.T) {
	type args struct {
		id uuid.UUID
	}
	tests := []struct {
		name         string
		args         args
		wantProfiles []domain.Profile
		wantErr      error
		setup        func(execer database.QueryerExecer)
	}{
		{
			name: "empty",
			args: args{
				id: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantProfiles: []domain.Profile{},
			wantErr:      nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}

				_, err = execer.Exec(`INSERT INTO followers (profile_id, follower_id)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', '7d2838fc-cbf8-4553-a671-474c591bcac1');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
		{
			name: "success",
			args: args{
				id: uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
			},
			wantProfiles: []domain.Profile{
				{
					ID:        uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac1"),
					FirstName: "test first name",
					LastName:  "test last name",
					City:      "city",
					Interests: "interests",
					Gender:    "male",
					BirthDate: nil,
				},
			},
			wantErr: nil,
			setup: func(execer database.QueryerExecer) {
				_, err := execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}
				_, err = execer.Exec(`INSERT INTO profiles (id, first_name, last_name, city, interests, gender)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac3', 'test first name', 'test last name', 'city', 'interests', 'male');
`)
				if err != nil {
					t.Fatal(err)
				}

				_, err = execer.Exec(`INSERT INTO followers (profile_id, follower_id)
VALUES ('7d2838fc-cbf8-4553-a671-474c591bcac1', '7d2838fc-cbf8-4553-a671-474c591bcac3');
`)
				if err != nil {
					t.Fatal(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tx, err := db.BeginTxx(context.Background(), nil)
			require.NoError(t, err)
			defer func() {
				err := tx.Rollback()
				require.NoError(t, err)
			}()
			tt.setup(tx)
			r := NewProfileRepository(tx, tx)
			gotProfiles, err := r.FindFollowings(tt.args.id)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.wantProfiles, gotProfiles)
		})
	}
}

func init() {
	dsn := os.Getenv("DSN")
	if dsn == "" {
		panic("require set end DSN")
	}
	var err error
	db, err = sqlx.Connect("mysql", dsn)
	if err != nil {
		panic(err)
	}
}
