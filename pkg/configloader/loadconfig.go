package configloader

import (
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
	"strings"
)

func LoadConfig(filePath string, out interface{}) error {
	if filePath != "" {
		viper.SetConfigFile(filePath)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			return err
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".socialnetwork")
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	// workaround because viper does not treat env vars the same as other config
	for _, key := range viper.AllKeys() {
		val := viper.Get(key)
		viper.Set(key, val)
	}

	return viper.Unmarshal(out)
}
