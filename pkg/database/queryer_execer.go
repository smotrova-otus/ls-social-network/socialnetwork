package database

import (
	"github.com/jmoiron/sqlx"
)

type QueryerExecer interface {
	sqlx.Queryer
	sqlx.Execer
}

type Queryer interface {
	sqlx.Queryer
}
