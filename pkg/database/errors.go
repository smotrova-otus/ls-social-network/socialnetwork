package database

import "errors"

var ErrNoFound = errors.New("not found")
