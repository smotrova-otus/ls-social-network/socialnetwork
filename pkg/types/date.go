package types

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type Date struct {
	time.Time
}

func (j *Date) Value() (driver.Value, error) {
	if j == nil {
		return nil, nil
	}
	return j.Time, nil
}

func (j *Date) Scan(src interface{}) error {
	if src == nil {
		return nil
	}
	t, ok := src.(time.Time)
	if !ok {
		return fmt.Errorf("value is not time.Time")
	}
	*j = Date{
		Time: t,
	}
	return nil
}

func (j *Date) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), "\"")
	t, err := time.Parse("2006-01-02", s)
	if err != nil {
		return fmt.Errorf("unmarshal failed: %w", err)
	}
	*j = Date{
		Time: t,
	}
	return nil
}

func (j Date) MarshalJSON() ([]byte, error) {
	return json.Marshal(j.Time)
}
