package migrate

import (
	"database/sql"
	"fmt"
	sqlmigrate "github.com/rubenv/sql-migrate"
	"time"
)

// Migrater runs migrations.
type Migrater interface {
	Up() (int, error)
	Down() (int, error)
	Status() error
}

// Migrater runs migrations.
// It implements Migrater.
type Migrates struct {
	migrationTableName string
	dialect            string
	source             sqlmigrate.MigrationSource
	db                 *sql.DB
}

// NewMigrater is a constructor.
func NewMigrater(db *sql.DB,
	migrationTableName, dialect string,
	source sqlmigrate.MigrationSource,
) *Migrates {
	return &Migrates{
		migrationTableName: migrationTableName,
		dialect:            dialect,
		source:             source,
		db:                 db,
	}
}

// Up runs migrations.
func (m Migrates) Up(limit int) (int, error) {
	count, err := sqlmigrate.ExecMax(
		m.db,
		m.dialect,
		m.source,
		sqlmigrate.Up,
		limit,
	)
	if err != nil {
		return 0, err
	}
	return count, nil
}

// Down rollbacks migrations.
func (m Migrates) Down(limit int) (int, error) {
	count, err := sqlmigrate.ExecMax(
		m.db,
		m.dialect,
		m.source,
		sqlmigrate.Down,
		limit,
	)
	if err != nil {
		return 0, err
	}

	return count, err
}

type StatusRow struct {
	ID            string
	Migrated      bool
	AppliedAt     time.Time
	RollbackCount int
	Error         string
}

// Status returns status of migrations.
func (m Migrates) Status() (map[string]*StatusRow, error) {
	// Get migration files.
	migrations, err := m.source.FindMigrations()
	if err != nil {
		return nil, err
	}

	// Get applied migrations from DB.
	records, err := sqlmigrate.GetMigrationRecords(m.db, m.dialect)
	if err != nil {
		return nil, err
	}

	rows := make(map[string]*StatusRow)

	for _, m := range migrations {
		rows[m.Id] = &StatusRow{
			ID:       m.Id,
			Migrated: false,
		}
	}

	for i, r := range records {
		if _, ok := rows[r.Id]; !ok {
			rows[r.Id] = &StatusRow{
				ID:       r.Id,
				Migrated: false,
				Error: fmt.Sprintf(
					"could not find migration file: %v\n",
					r.Id,
				),
			}
			continue
		}
		rows[r.Id].RollbackCount = len(records) - i
		rows[r.Id].Migrated = true
		rows[r.Id].AppliedAt = r.AppliedAt
	}

	return rows, nil
}
