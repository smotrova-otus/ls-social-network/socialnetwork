package http

import (
	"context"
	"encoding/json"
	"github.com/go-kit/kit/auth/jwt"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"net/http"

	kithttp "github.com/go-kit/kit/transport/http"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
)

// ErrorEncoder is responsible for encoding an error to the ResponseWriter.
// For errors without json.Marshaller returns Internal error.
func ErrorEncoder(ctx context.Context, err error, w http.ResponseWriter) {
	log.Must(ctx).WithError(err).Info("error encoding")

	if err == jwt.ErrTokenContextMissing ||
		err == jwt.ErrTokenInvalid ||
		err == jwt.ErrTokenExpired ||
		err == jwt.ErrTokenMalformed ||
		err == jwt.ErrTokenNotActive ||
		err == jwt.ErrUnexpectedSigningMethod {
		err = errors.ErrUnauthorized
	}

	// go-kit.DefaultErrorEncoder doesn't respond with json
	// for errors which is not implement json.Marshaler,
	// for those type of errors return InternalError.
	// go-kit/kit@v0.8.0/transport/http/server.go:180
	if _, ok := err.(json.Marshaler); !ok {
		err = errors.ErrInternal
	}

	kithttp.DefaultErrorEncoder(ctx, err, w)
}
