package seed

import (
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/types"
	"math/rand"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/auth"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/profile"

	"github.com/bxcodec/faker/v3"
)

type ProfilesSeeder struct {
	accountsRepository auth.AccountRepository
	profilesRepository profile.Repository
	uuidGenerator      uuid.Generator
	logger             *logrus.Entry
}

func NewProfilesSeeder(
	accountsRepository auth.AccountRepository,
	profilesRepository profile.Repository,
	uuidGenerator uuid.Generator,
	logger *logrus.Entry,
) *ProfilesSeeder {
	return &ProfilesSeeder{
		accountsRepository: accountsRepository,
		profilesRepository: profilesRepository,
		uuidGenerator:      uuidGenerator,
		logger:             logger,
	}
}

func (s ProfilesSeeder) Run(count int) {
	createdCount := 0
	for i := 0; i < count; i++ {
		err := s.CreateProfile()
		if err != nil {
			s.logger.Errorf("failed create profile, created count  %v profiles", createdCount)
			continue
		}
		createdCount++
		if i%100 == 0 {
			s.logger.Infof("created %v profiles", createdCount)
		}
	}
	s.logger.Infof("created %v profiles", createdCount)
}

func (s ProfilesSeeder) CreateProfile() error {
	p := s.generateProfile()
	err := s.profilesRepository.Insert(&p)
	if err != nil {
		s.logger.WithError(err).Errorf("create profile failed %v", p)
		return err
	}

	return nil
}

func (s ProfilesSeeder) generateProfile() domain.Profile {
	genders := []domain.GenderType{
		domain.GenderTypeFemale,
		domain.GenderTypeMale,
	}
	var bd *types.Date
	t, err := time.Parse("2006-01-02", faker.Date())
	if err == nil {
		bd = &types.Date{
			Time: t,
		}
	}
	return domain.Profile{
		ID:        s.uuidGenerator.New(),
		FirstName: faker.FirstName(),
		LastName:  faker.LastName(),
		BirthDate: bd,
		Gender:    genders[rand.Int()%len(genders)],
		Interests: faker.Sentence(),
		City:      faker.Word(),
	}
}
