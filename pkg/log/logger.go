package log

import (
	"context"
	kithttp "github.com/go-kit/kit/transport/http"
	"net/http"

	"github.com/sirupsen/logrus"
)

const (
	loggerContextKey = "gitlab.com/smotrova-otus/ls-social-network/socialnetwork_logger"
)

func Must(ctx context.Context) *logrus.Entry {
	logger := ctx.Value(loggerContextKey)
	l, ok := logger.(*logrus.Entry)
	if !ok {
		panic("not found logger in context")
	}
	return l
}

func ContextWithLogger(ctx context.Context, logger *logrus.Entry) context.Context {
	return context.WithValue(ctx, loggerContextKey, logger)
}

func NewLoggerRequestMiddleware(logger *logrus.Entry) kithttp.RequestFunc {
	return func(ctx context.Context, _ *http.Request) context.Context {
		return ContextWithLogger(ctx, logger)
	}
}

func Default() (context.Context, *logrus.Entry) {
	l := logrus.NewEntry(logrus.New())
	ctx := ContextWithLogger(context.Background(), l)
	return ctx, l
}
