package auth

import (
	"context"
	"fmt"
	stdjwt "github.com/dgrijalva/jwt-go"
	"github.com/go-kit/kit/auth/jwt"
	kitendpoint "github.com/go-kit/kit/endpoint"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth/claimsvalidation"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth/token"
)

type JwtSigningString string

// EndpointParams provides params for endpoint middlewares.
type EndpointParams struct {
	JWTStrategyConfig token.JWTStrategyConfig
}

// AuthenticationMiddleware wraps endpoint with standard middlewares.
func AuthenticationMiddleware(params EndpointParams) kitendpoint.Middleware {
	return func(endpoint kitendpoint.Endpoint) kitendpoint.Endpoint {
		endpoint = JWTValidateMiddleware(params.JWTStrategyConfig)(endpoint)
		return endpoint
	}
}

func JWTValidateMiddleware(config token.JWTStrategyConfig) kitendpoint.Middleware {
	return func(endpoint kitendpoint.Endpoint) kitendpoint.Endpoint {
		endpoint = claimsvalidation.NewMiddleware(
			config.Issuer,
			config.Audience,
		)(endpoint)

		kf := func(token *stdjwt.Token) (interface{}, error) {
			return config.Key, nil
		}
		endpoint = jwt.NewParser(kf, stdjwt.SigningMethodHS256, token.ClaimsFactory)(endpoint)

		return endpoint
	}
}

func CurrentUser(ctx context.Context) (*domain.Account, error) {
	tokenClaims, ok := ctx.Value(jwt.JWTClaimsContextKey).(*token.Claims) // TODO use claims factory
	if !ok {
		return nil, fmt.Errorf("token claims not found in context")
	}

	return &domain.Account{
		ID:    tokenClaims.SubjectID,
		Login: tokenClaims.Subject,
	}, nil
}
