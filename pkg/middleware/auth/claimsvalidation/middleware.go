package claimsvalidation

import (
	"context"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/errors"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/log"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth/token"

	kitjwt "github.com/go-kit/kit/auth/jwt"
	"github.com/go-kit/kit/endpoint"
)

func NewMiddleware(
	expectedIssuer,
	expectedAudience string,
) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			logger := log.Must(ctx)
			logger.WithField("middleware", "claimsvalidation")
			tokenClaims, ok := ctx.Value(kitjwt.JWTClaimsContextKey).(*token.Claims) // TODO use claims factory
			if !ok {
				logger.Warn("access denied. Token claims was not passed through the context")
				return nil, kitjwt.ErrTokenContextMissing
			}

			if !tokenClaims.VerifyIssuer(expectedIssuer, true) {
				logger.Warnf(
					"access denied. Token has incorrect issuer,expected:%s, actual:%s",
					expectedIssuer,
					tokenClaims.Issuer,
				)
				return nil, errors.ErrUnauthorized
			}
			if !tokenClaims.VerifyAudience(expectedAudience, true) {
				logger.Warnf(
					"access denied. Token has incorrect audience,expected:%s, actual:%s",
					expectedAudience,
					tokenClaims.Audience,
				)
				return nil, errors.ErrUnauthorized
			}

			logger.Info(
				"access allowed",
			)
			return next(ctx, request)
		}
	}
}
