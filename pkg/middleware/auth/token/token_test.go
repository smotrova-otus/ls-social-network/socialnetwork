package token

import (
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/clock"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestServiceJwt_MakeToken(t *testing.T) {
	clockMock := &clock.ClockMock{}
	now, err := time.Parse(time.RFC3339, "2006-01-02T15:04:05.000Z")
	if err != nil {
		t.Fatal(err)
	}
	clockMock.On("Now").Return(now)
	type fields struct {
		cfg JWTStrategyConfig
	}
	type args struct {
		subject domain.Account
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr error
	}{
		{
			name: "success",
			fields: fields{
				cfg: JWTStrategyConfig{
					Key:      []byte("secret_key"),
					Ttl:      time.Second * 10,
					Audience: "api.com",
					Issuer:   "auth.com",
				},
			},
			args: args{
				subject: domain.Account{
					ID:    uuid.MustParse("7d2838fc-cbf8-4553-a671-474c591bcac3"),
					Login: "test@gmail.com",
				},
			},
			want:    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhcGkuY29tIiwiZXhwIjoxMTM2MjE0MjU1LCJpc3MiOiJhdXRoLmNvbSIsInN1YiI6InRlc3RAZ21haWwuY29tIiwic3ViSWQiOiI3ZDI4MzhmYy1jYmY4LTQ1NTMtYTY3MS00NzRjNTkxYmNhYzMifQ.if0GnVxtK-IwWRNL9LqOlRi3uDHtEm6BD-izi_1qQvU",
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewJwtService(tt.fields.cfg, clockMock)
			got, err := s.MakeToken(tt.args.subject)
			require.Equal(t, tt.wantErr, err)
			require.Equal(t, tt.want, got)
		})
	}
}
