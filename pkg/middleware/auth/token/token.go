package token

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/domain"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"time"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/clock"
)

var (
	ErrSignToken = errors.New("fail when sign token")
)

// Creator is interface for creating token.
type Creator interface {
	MakeToken(acc domain.Account) (string, error)
}

// JWTStrategyConfig is config for jwt service.
type JWTStrategyConfig struct {
	// Key for token signing.
	Key []byte

	// Token time life.
	Ttl time.Duration

	// Token audience.
	Audience string

	// Token Issuer.
	Issuer string
}

// NewJwtService creates jst service.
func NewJwtService(cfg JWTStrategyConfig, clock clock.Clock) *ServiceJwt {
	return &ServiceJwt{
		cfg:   cfg,
		clock: clock,
	}
}

// ServiceJwt is jwt implementation.
type ServiceJwt struct {
	cfg   JWTStrategyConfig
	clock clock.Clock
}

type Claims struct {
	jwt.StandardClaims
	SubjectID uuid.UUID `json:"subId,omitempty"`
}

func ClaimsFactory() jwt.Claims {
	return &Claims{}
}

// MakeToken creates token.
func (s *ServiceJwt) MakeToken(acc domain.Account) (string, error) {
	expirationTime := s.clock.Now().Add(s.cfg.Ttl)

	claims := &Claims{
		StandardClaims: jwt.StandardClaims{
			Audience:  s.cfg.Audience,
			ExpiresAt: expirationTime.Unix(),
			Issuer:    s.cfg.Issuer,
			Subject:   acc.Login,
		},
		SubjectID: acc.ID,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString(s.cfg.Key)
	if err != nil {
		return "", fmt.Errorf("%w: %v", ErrSignToken, err)
	}

	return tokenString, nil
}
