package uuid

import (
	"github.com/stretchr/testify/mock"
)

// GeneratorMock is a mock for Generator
type GeneratorMock struct {
	mock.Mock
}

// NewGeneratorMock is a constrictor.
func NewGeneratorMock() *GeneratorMock {
	return &GeneratorMock{}
}

// New implementation.
func (m *GeneratorMock) New() UUID {
	results := m.Called()
	return results.Get(0).(UUID)
}

// SetNew set mock for method New.
func (m *GeneratorMock) SetNew(id UUID) {
	m.On("New").
		Return(id)
}
