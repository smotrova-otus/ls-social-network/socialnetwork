package uuid

import (
	"database/sql/driver"
	google "github.com/google/uuid"
)

type UUID google.UUID

// Parse converts string to uuid.
func Parse(value string) (UUID, error) {
	id, err := google.Parse(value)
	if err != nil {
		return UUID(google.Nil), err
	}
	return UUID(id), nil
}

// MustParse is like Parse but panics if the string cannot be parsed.
func MustParse(s string) UUID {
	uuid, err := Parse(s)
	if err != nil {
		panic(`uuid: Parse(` + s + `): ` + err.Error())
	}
	return uuid
}

// Scan implements sql.Scanner so UUIDs can be read from databases transparently
// Currently, database types that map to string and []byte are supported. Please
// consult database-specific driver documentation for matching types.
func (id *UUID) Scan(src interface{}) error {
	return (*google.UUID)(id).Scan(src)
}

// Value implements sql.Valuer so that UUIDs can be written to databases
// transparently. Currently, UUIDs map to strings. Please consult
// database-specific driver documentation for matching types.
func (id UUID) Value() (driver.Value, error) {
	return google.UUID(id).Value()
}

// MarshalText implements encoding.TextMarshaler.
func (id UUID) MarshalText() ([]byte, error) {
	return google.UUID(id).MarshalText()
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (id *UUID) UnmarshalText(data []byte) error {
	u := (*google.UUID)(id)
	return u.UnmarshalText(data)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (id UUID) MarshalBinary() ([]byte, error) {
	return google.UUID(id).MarshalBinary()
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (id *UUID) UnmarshalBinary(data []byte) error {
	u := google.UUID(*id)
	return u.UnmarshalBinary(data)
}
