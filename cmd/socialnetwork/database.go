package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/config"
)

type MasterConnection struct {
	*sqlx.DB
}

type SlaveConnection struct {
	*sqlx.DB
}

func connectToDatabase(dsn string) (*sqlx.DB, error) {
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(100)
	return db, nil
}

func calculateDSN(config config.DBConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?parseTime=true",
		config.User,
		config.Password,
		config.Host,
		config.Port,
		config.Name,
	)
}
