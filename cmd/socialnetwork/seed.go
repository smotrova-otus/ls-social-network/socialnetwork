package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/auth"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/seed"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/profile"
)

// rootSeedCmd.
var rootSeedCmd = &cobra.Command{
	Use:          "seed",
	Short:        "seed",
	Long:         "",
	SilenceUsage: true,
}

var butchProfilesCMD = &cobra.Command{
	Use:   "profiles",
	Short: "",
	RunE: func(cmd *cobra.Command, args []string) error {
		err := masterConn.DB.Ping()
		if err != nil {
			return err
		}
		err = slaveConn.DB.Ping()
		if err != nil {
			return err
		}
		seeder := seed.NewProfilesSeeder(
			auth.NewAccountRepository(masterConn),
			profile.NewProfileRepository(masterConn, slaveConn),
			uuid.NewGeneratorImpl(),
			logger,
		)
		seeder.Run(1000000)
		return nil
	},
	SilenceUsage: true,
}

func init() {
	rootCmd.AddCommand(rootSeedCmd)
	rootSeedCmd.AddCommand(butchProfilesCMD)
}
