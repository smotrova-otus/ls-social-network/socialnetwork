//+build wireinject

package main

import (
	"github.com/google/wire"
	"github.com/sirupsen/logrus"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/auth"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/config"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/passwordhash"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/clock"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/database"
	auth2 "gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/middleware/auth/token"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/uuid"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/profile"
)

func NewContainer(
	cfg *config.Config,
	logger *logrus.Entry,
	masterConn *MasterConnection,
	slaveConn *SlaveConnection,
) *Container {
	panic(wire.Build(
		auth.WireSet,
		profile.WireSet,

		token.NewJwtService,
		wire.Bind(new(token.Creator), new(*token.ServiceJwt)),
		provideJwtStrategyConfig,

		passwordhash.NewPasswordHasherImpl,
		wire.Bind(new(passwordhash.PasswordHasher), new(*passwordhash.PasswordHasherImpl)),

		uuid.NewGeneratorImpl,
		wire.Bind(new(uuid.Generator), new(*uuid.GeneratorImpl)),

		clock.NewRealClock,
		wire.Bind(new(clock.Clock), new(*clock.RealClock)),

		wire.Bind(new(database.QueryerExecer), new(*MasterConnection)),
		wire.Bind(new(database.Queryer), new(*SlaveConnection)),

		wire.Struct(new(auth2.EndpointParams), "*"),
		wire.Struct(new(Container), "*"),
	))
}

func provideJwtStrategyConfig(cfg *config.Config) token.JWTStrategyConfig {
	return token.JWTStrategyConfig{
		Key:      []byte(cfg.Auth.Key),
		Ttl:      cfg.Auth.Ttl,
		Audience: cfg.Auth.Audience,
		Issuer:   cfg.Auth.Issuer,
	}
}
