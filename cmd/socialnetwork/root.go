package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/config"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/pkg/configloader"
)

var (
	cfgFile    string
	masterConn *MasterConnection
	slaveConn  *SlaveConnection
	logger     *logrus.Entry
	cfg        = new(config.Config)
)

var preRunHook = func(cmd *cobra.Command, _ []string) error {
	err := configloader.LoadConfig(cfgFile, cfg)
	if err != nil {
		return fmt.Errorf("config loading failed: %w", err)
	}

	logger = initLogger(*cfg)

	if conn, err := connectToDatabase(calculateDSN(*cfg.Database.Master)); err == nil {
		masterConn = &MasterConnection{conn}
	} else {
		return fmt.Errorf("connecting to database failed: %w", err)
	}

	// If slave is not specified just use the master connection only.
	if cfg.Database.Slave == nil {
		logger.Info("slave configuration is not specified, master connection will be used")
		slaveConn = &SlaveConnection{masterConn.DB}
		return nil
	}

	if conn, err := connectToDatabase(calculateDSN(*cfg.Database.Slave)); err == nil {
		logger.Infof("slave configuration is specified, %v", cfg.Database.Slave)
		slaveConn = &SlaveConnection{conn}
	} else {
		return fmt.Errorf("connecting to database failed: %w", err)
	}
	return nil
}

var postRunHook = func(cmd *cobra.Command, _ []string) error {
	_ = masterConn.Close()
	_ = slaveConn.Close()
	return nil
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:                "socialnetwork",
	Short:              "A brief description of your application",
	Long:               "",
	PersistentPreRunE:  preRunHook,
	PersistentPostRunE: postRunHook,
	Run:                serve,
	SilenceUsage:       true,
}

func serve(cmd *cobra.Command, args []string) {
	container := NewContainer(cfg, logger, masterConn, slaveConn)

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()
	httpAddr := cfg.Transport.Http.Address
	go func() {
		logger.Infof("server start at %s", httpAddr)
		errs <- http.ListenAndServe(httpAddr, container.Handler())
	}()

	logger.Errorf("start server failed: %v", <-errs)
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.socialnetwork.yaml)")
}
