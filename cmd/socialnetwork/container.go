package main

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/auth"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/frontend"
	"gitlab.com/smotrova-otus/ls-social-network/socialnetwork/profile"
	"net/http"
	"os"
)

type Container struct {
	AuthHTTPHandlerSet    auth.HTTPHandlerSet
	ProfileHTTPHandlerSet profile.HTTPHandlerSet
}

func (h Container) Handler() http.Handler {
	router := mux.NewRouter()

	frontend.Register(router)
	h.AuthHTTPHandlerSet.Register(router)
	h.ProfileHTTPHandlerSet.Register(router)

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{os.Getenv("ORIGIN_ALLOWED")})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	router.Handle("/", http.RedirectHandler("/ui/", http.StatusMovedPermanently))

	return handlers.CORS(originsOk, headersOk, methodsOk)(router)
}
