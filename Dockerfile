# build stage
FROM golang:alpine AS build-env

RUN apk update && apk add git gcc libc-dev

WORKDIR /app

COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build -a -o main ./cmd/socialnetwork/

# final stage
FROM alpine:latest

RUN apk update && apk add ca-certificates && rm -fr /var/cache/apk/*

WORKDIR /app

COPY --from=build-env /app/main .
# TODO: extract config into docker env variables
COPY --from=build-env /app/config.yml .

ENTRYPOINT ["./main"]

CMD ["-c", "./config.yml"]